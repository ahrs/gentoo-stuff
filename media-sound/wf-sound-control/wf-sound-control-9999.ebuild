# Copyright 2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="A small GUI utility to control sound volume"
HOMEPAGE="https://github.com/WayfireWM/wf-sound-control"

EGIT_REPO_URI="${HOMEPAGE}.git"

inherit git-r3 meson

LICENSE="MIT"
SLOT="0"
KEYWORDS=""
IUSE=""

DEPEND="
	dev-cpp/gtkmm:3.0
	media-libs/alsa-lib
	dev-libs/wayland
"
RDEPEND="
	${DEPEND}
	gui-wm/wayfire
"
BDEPEND="dev-libs/wayland-protocols"

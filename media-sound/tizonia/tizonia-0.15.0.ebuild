# Copyright 2018 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

PYTHON_COMPAT=( python2_7 )

inherit autotools distutils-r1 eutils

DESCRIPTION="Command-line cloud music player for Linux"
HOMEPAGE="https://www.tizonia.org"

_githubname=tizonia-openmax-il
SRC_URI="https://github.com/tizonia/${_githubname}/archive/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="LGPL-3"
SLOT="0"
KEYWORDS=""
IUSE=""

DEPEND="
	>sys-apps/dbus-0.6.1
	media-libs/libmad:=
	dev-db/sqlite:=
	sys-apps/util-linux:=
	media-libs/taglib:=
	media-video/mediainfo:=
	media-libs/libsdl:=
	media-sound/lame:=
	media-libs/faad2:=
	net-misc/curl[ssl]`# may specifically require gnutls 'libcurl-gnutls'`
	media-libs/libvorbis:=
	media-libs/libvpx:=
	media-sound/mpg123:=
	media-libs/opus:=
	media-libs/opusfile:=
	media-libs/libogg:=
	media-libs/libfishsound:=
	media-libs/flac:=
	media-libs/liboggz:=
	media-libs/libsndfile:=
	media-libs/alsa-lib:=
	media-sound/pulseaudio:=
	dev-libs/boost[python]
	dev-libs/check:=
	dev-python/pafy[$PYTHON_USEDEP]
	dev-python/eventlet[$PYTHON_USEDEP]
	dev-libs/log4c:=
	media-libs/libspotify:=
	`# 'python2-gmusicapi'`
	dev-python/soundcloud[$PYTHON_USEDEP]
	net-misc/youtube-dl[$PYTHON_USEDEP]
	`# 'python2-pychromecast-git'`
	`# 'python-plexapi'`
	dev-python/fuzzywuzzy[$PYTHON_USEDEP]
	`# 'python2-spotipy'`
"
RDEPEND="${DEPEND}"
BDEPEND=""

S="${WORKDIR}/${_githubname}-${PV}"

src_prepare() {
	mkdir -p "$T/path"
	ln -sf /usr/bin/python2.7 "$T/path/python"
	ln -sf /usr/bin/python2.7-config "$T/path/python-config"
	export PATH="$T/path:$PATH"
	export PYTHON="/usr/bin/python2"
	eautoreconf || die

	default
}

src_configure() {
	mkdir -p "$T/path"
	ln -sf /usr/bin/python2.7 "$T/path/python"
	ln -sf /usr/bin/python2.7-config "$T/path/python-config"
	export PATH="$T/path:$PATH"
	export PYTHON="/usr/bin/python2"
	find . -type f -name "*.pc" -exec /bin/sh -c 'grep -q \\-lboost_python {} && echo {}' \; | while read -r line; do sed -i 's|-lboost_python|-lboost_python-2.7|g' "$line"; done
	./configure \
		--prefix=/usr \
		--sysconfdir=/etc \
		--localstatedir=/var || die
	# This is terrible
	# To Do:
	#	* Fix Me
	find . -type f -name Makefile -exec /bin/sh -c 'grep -q \\-lboost_python {} && echo {}' \; | while read -r line; do sed -i 's|-lboost_python|-lboost_python-2.7|g' "$line"; done
}

python_compile() {
	mkdir -p "$T/path"
	ln -sf /usr/bin/python2.7 "$T/path/python"
	ln -sf /usr/bin/python2.7-config "$T/path/python-config"
	export PATH="$T/path:$PATH"
	export PYTHON="/usr/bin/python2"
	emake PYTHON="/usr/bin/python2.7"
}

src_install() {
	emake install DESTDIR="${D}"
}

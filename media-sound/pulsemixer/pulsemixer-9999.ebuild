# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

PYTHON_COMPAT=( python3_{4,5,6,7} )

inherit distutils-r1 git-r3

DESCRIPTION="cli and curses mixer for pulseaudio"
HOMEPAGE="https://github.com/GeorgeFilipkin/pulsemixer"
#SRC_URI="https://github.com/GeorgeFilipkin/pulsemixer/archive/1.3.0-license.tar.gz"
SRC_URI=""
EGIT_REPO_URI="https://github.com/GeorgeFilipkin/pulsemixer"
EGIT_BRANCH="master"

LICENSE="MIT"
SLOT="0"
KEYWORDS=""
IUSE="pulseaudio"

RDEPEND="
		media-sound/pulseaudio
"
DEPEND="${RDEPEND}
	dev-python/setuptools[${PYTHON_USEDEP}]
"

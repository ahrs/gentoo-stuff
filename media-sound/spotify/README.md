# spotify

A slight modification to the Gentoo ebuild for spotify. All openssl dependencies have been removed so that I can emerge this with my libressl systems. A proper fix would be to introduce a libressl USE flag or make it depend on either openssl or libressl with the appropriate USE flags for net-misc/curl too.

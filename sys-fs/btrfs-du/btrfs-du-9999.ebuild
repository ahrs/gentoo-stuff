# Copyright 2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Easily print BTRFS subvolume/snapshot disk usage"
HOMEPAGE="https://github.com/nachoparker/btrfs-du"

EGIT_REPO_URI="${HOMEPAGE}.git"

inherit git-r3

LICENSE="GPL-3"
SLOT="0"
KEYWORDS=""
IUSE=""

DEPEND=""
RDEPEND="
	app-shells/bash
	>=sys-fs/btrfs-progs-3.12
	${DEPEND}"
BDEPEND=""

src_compile() { :; }

src_install() {
	[[ -f "README.md" ]] && dodoc "README.md"
	dobin "${PN}" || die "${PN} not found"
}

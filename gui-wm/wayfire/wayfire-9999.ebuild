# Copyright 2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Customisable wayland compositor based on wlroots"
HOMEPAGE="https://github.com/WayfireWM/wayfire"

inherit git-r3 meson

EGIT_REPO_URI="${HOMEPAGE}.git"

LICENSE="MIT"
SLOT="0"
KEYWORDS=""
IUSE="shell"

DEPEND="
	media-libs/glm
	gui-libs/wlroots
	gui-libs/wf-config
"
RDEPEND="
	${DEPEND}
	shell? ( gui-apps/wf-shell )
"
BDEPEND=""

src_unpack() {
	if has_version ">=gui-libs/wlroots-9999"
	then
		# Use the wlroots tracking branch with bleeding edge wlroots
		export EGIT_BRANCH=track-wlroots
	fi
	git-r3_src_unpack
}

src_install() {
	meson_src_install
	dodoc "${PN}.ini.default"
}

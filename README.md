# Gentoo Stuff

This is my local Gentoo repo that may or may not be maintained and kept up to date.

It contains third-party software I use and any patched ebuilds I'm using (for example to fix LibreSSL related errors)

## Usage

### eselect-repository

This is the preferred way of managing overlays:

#### Install eselect-repository

```
emerge -av app-eselect/eselect-repository
```

#### Add the overlay

```
eselect repository add ahrs git https://gitlab.com/ahrs/gentoo-stuff.git
emerge --sync ahrs
```

### Manually

Add the following to `/etc/portage/repos.conf/ahrs.conf`:

```
[ahrs]
location = /var/db/repos/ahrs
sync-type = git
sync-uri = https://gitlab.com/ahrs/gentoo-stuff.git
```

Sync the overlay:

`emerge --sync ahrs`

## Third-Party Overlays

For the sake of de-duplication some packages depend on packages provided by other overlays. These are documented in the masters section of [layout.conf](https://gitlab.com/ahrs/gentoo-stuff/blob/master/metadata/layout.conf) but a more thorough description is given below:

### [Go-overlay](https://github.com/Dr-Terrible/go-overlay)

This overlay provides golang related packages

`eselect repository enable go-overlay`

**Needed by:**

* [www-client/browsh](https://gitlab.com/ahrs/gentoo-stuff/tree/master/www-client/browsh)

### [Rust](https://github.com/gentoo/gentoo-rust)

This overlay provides newer rust packages that haven't made it into the Gentoo tree yet.

`eselect repository enable rust`

**Needed by:**

* [app-text/mdbook](https://gitlab.com/ahrs/gentoo-stuff/tree/master/app-text/mdbook)
* [app-shells/ion](https://gitlab.com/ahrs/gentoo-stuff/tree/master/app-shells/ion)

### [Atom](https://github.com/elprans/atom-overlay)

This is only needed if you intend to compile electron from source instead of using the binaries provided by [electron-bin](https://gitlab.com/ahrs/gentoo-stuff/tree/master/dev-util/electron-bin/). If you find a package that depends on electron pulling in the source version and you'd prefer to use the binary you can force it as follows:

```
emerge -av1 dev-util/electron-bin:slot-number virtual/electron:slot-number
```

Where slot number is the version of electron e.g:

```
emerge -av1 virtual/electron:2.0 dev-util/electron-bin:2.0
```

**Needed by:**

* [virtual/electron](https://gitlab.com/ahrs/gentoo-stuff/tree/master/virtual/electron)

---

<!-- ::BEGIN_TREE:: -->
├──
[app-admin](https://gitlab.com/ahrs/gentoo-stuff/tree/master/app-admin/)  
│   ├──
[pscircle](https://gitlab.com/ahrs/gentoo-stuff/tree/master/app-admin/pscircle/)  
│   ├──
[qtpass](https://gitlab.com/ahrs/gentoo-stuff/tree/master/app-admin/qtpass/)  
│   ├──
[salt](https://gitlab.com/ahrs/gentoo-stuff/tree/master/app-admin/salt/)  
│   ├──
[vrms](https://gitlab.com/ahrs/gentoo-stuff/tree/master/app-admin/vrms/)  
│   └──
[xkcdpass](https://gitlab.com/ahrs/gentoo-stuff/tree/master/app-admin/xkcdpass/)  
├──
[app-arch](https://gitlab.com/ahrs/gentoo-stuff/tree/master/app-arch/)  
│   └──
[dtrx](https://gitlab.com/ahrs/gentoo-stuff/tree/master/app-arch/dtrx/)  
├──
[app-benchmarks](https://gitlab.com/ahrs/gentoo-stuff/tree/master/app-benchmarks/)  
│   └──
[unigine-valley](https://gitlab.com/ahrs/gentoo-stuff/tree/master/app-benchmarks/unigine-valley/)  
├──
[app-doc](https://gitlab.com/ahrs/gentoo-stuff/tree/master/app-doc/)  
│   ├──
[dasht](https://gitlab.com/ahrs/gentoo-stuff/tree/master/app-doc/dasht/)  
│   ├──
[rfc](https://gitlab.com/ahrs/gentoo-stuff/tree/master/app-doc/rfc/)  
│   └──
[zeal](https://gitlab.com/ahrs/gentoo-stuff/tree/master/app-doc/zeal/)  
├──
[app-editors](https://gitlab.com/ahrs/gentoo-stuff/tree/master/app-editors/)  
│   ├──
[neovim-gtk](https://gitlab.com/ahrs/gentoo-stuff/tree/master/app-editors/neovim-gtk/)  
│   └──
[vscode](https://gitlab.com/ahrs/gentoo-stuff/tree/master/app-editors/vscode/)  
├──
[app-eselect](https://gitlab.com/ahrs/gentoo-stuff/tree/master/app-eselect/)  
│   └──
[eselect-electron](https://gitlab.com/ahrs/gentoo-stuff/tree/master/app-eselect/eselect-electron/)  
├──
[app-misc](https://gitlab.com/ahrs/gentoo-stuff/tree/master/app-misc/)  
│   ├──
[anki-bin](https://gitlab.com/ahrs/gentoo-stuff/tree/master/app-misc/anki-bin/)  
│   ├──
[bcal](https://gitlab.com/ahrs/gentoo-stuff/tree/master/app-misc/bcal/)  
│   ├──
[ddgr](https://gitlab.com/ahrs/gentoo-stuff/tree/master/app-misc/ddgr/)  
│   ├──
[dvtm](https://gitlab.com/ahrs/gentoo-stuff/tree/master/app-misc/dvtm/)  
│   ├──
[etcher](https://gitlab.com/ahrs/gentoo-stuff/tree/master/app-misc/etcher/)  
│   ├──
[fasd](https://gitlab.com/ahrs/gentoo-stuff/tree/master/app-misc/fasd/)  
│   ├──
[fff](https://gitlab.com/ahrs/gentoo-stuff/tree/master/app-misc/fff/)  
│   ├──
[mako](https://gitlab.com/ahrs/gentoo-stuff/tree/master/app-misc/mako/)  
│   ├──
[nnn](https://gitlab.com/ahrs/gentoo-stuff/tree/master/app-misc/nnn/)  
│   ├──
[ranger](https://gitlab.com/ahrs/gentoo-stuff/tree/master/app-misc/ranger/)  
│   ├──
[slurp](https://gitlab.com/ahrs/gentoo-stuff/tree/master/app-misc/slurp/)  
│   ├──
[tty-clock](https://gitlab.com/ahrs/gentoo-stuff/tree/master/app-misc/tty-clock/)  
│   └──
[waybar](https://gitlab.com/ahrs/gentoo-stuff/tree/master/app-misc/waybar/)  
├──
[app-mpv](https://gitlab.com/ahrs/gentoo-stuff/tree/master/app-mpv/)  
│   └──
[mpv-mpris](https://gitlab.com/ahrs/gentoo-stuff/tree/master/app-mpv/mpv-mpris/)  
├──
[app-office](https://gitlab.com/ahrs/gentoo-stuff/tree/master/app-office/)  
│   └──
[sc-im](https://gitlab.com/ahrs/gentoo-stuff/tree/master/app-office/sc-im/)  
├──
[app-portage](https://gitlab.com/ahrs/gentoo-stuff/tree/master/app-portage/)  
│   └──
[eclass-manpages](https://gitlab.com/ahrs/gentoo-stuff/tree/master/app-portage/eclass-manpages/)  
├──
[app-shells](https://gitlab.com/ahrs/gentoo-stuff/tree/master/app-shells/)  
│   ├──
[ion](https://gitlab.com/ahrs/gentoo-stuff/tree/master/app-shells/ion/)  
│   └──
[smenu](https://gitlab.com/ahrs/gentoo-stuff/tree/master/app-shells/smenu/)  
├──
[app-text](https://gitlab.com/ahrs/gentoo-stuff/tree/master/app-text/)  
│   ├──
[boostnote](https://gitlab.com/ahrs/gentoo-stuff/tree/master/app-text/boostnote/)  
│   ├──
[mdbook](https://gitlab.com/ahrs/gentoo-stuff/tree/master/app-text/mdbook/)  
│   ├──
[proselint](https://gitlab.com/ahrs/gentoo-stuff/tree/master/app-text/proselint/)  
│   └──
[qownnotes](https://gitlab.com/ahrs/gentoo-stuff/tree/master/app-text/qownnotes/)  
├──
[app-vscode](https://gitlab.com/ahrs/gentoo-stuff/tree/master/app-vscode/)  
│   └──
[vscodevim](https://gitlab.com/ahrs/gentoo-stuff/tree/master/app-vscode/vscodevim/)  
├──
[dev-dotnet](https://gitlab.com/ahrs/gentoo-stuff/tree/master/dev-dotnet/)  
│   └──
[dotnetcore-sdk-bin](https://gitlab.com/ahrs/gentoo-stuff/tree/master/dev-dotnet/dotnetcore-sdk-bin/)  
├──
[dev-libs](https://gitlab.com/ahrs/gentoo-stuff/tree/master/dev-libs/)  
│   ├──
[eglexternalplatform](https://gitlab.com/ahrs/gentoo-stuff/tree/master/dev-libs/eglexternalplatform/)  
│   ├──
[egl-wayland](https://gitlab.com/ahrs/gentoo-stuff/tree/master/dev-libs/egl-wayland/)  
│   ├──
[log4c](https://gitlab.com/ahrs/gentoo-stuff/tree/master/dev-libs/log4c/)  
│   ├──
[wayfire](https://gitlab.com/ahrs/gentoo-stuff/tree/master/dev-libs/wayfire/)  
│   ├──
[wf-config](https://gitlab.com/ahrs/gentoo-stuff/tree/master/dev-libs/wf-config/)  
│   └──
[wf-shell](https://gitlab.com/ahrs/gentoo-stuff/tree/master/dev-libs/wf-shell/)  
├──
[dev-python](https://gitlab.com/ahrs/gentoo-stuff/tree/master/dev-python/)  
│   ├──
[click-datetime](https://gitlab.com/ahrs/gentoo-stuff/tree/master/dev-python/click-datetime/)  
│   ├──
[glad](https://gitlab.com/ahrs/gentoo-stuff/tree/master/dev-python/glad/)  
│   ├──
[neovim-python-client](https://gitlab.com/ahrs/gentoo-stuff/tree/master/dev-python/neovim-python-client/)  
│   ├──
[pyhs100](https://gitlab.com/ahrs/gentoo-stuff/tree/master/dev-python/pyhs100/)  
│   ├──
[scfbuild](https://gitlab.com/ahrs/gentoo-stuff/tree/master/dev-python/scfbuild/)  
│   ├──
[soundcloud](https://gitlab.com/ahrs/gentoo-stuff/tree/master/dev-python/soundcloud/)  
│   └──
[typing](https://gitlab.com/ahrs/gentoo-stuff/tree/master/dev-python/typing/)  
├──
[dev-util](https://gitlab.com/ahrs/gentoo-stuff/tree/master/dev-util/)  
│   ├──
[electron-bin](https://gitlab.com/ahrs/gentoo-stuff/tree/master/dev-util/electron-bin/)  
│   ├──
[svgo](https://gitlab.com/ahrs/gentoo-stuff/tree/master/dev-util/svgo/)  
│   └──
[web-ext](https://gitlab.com/ahrs/gentoo-stuff/tree/master/dev-util/web-ext/)  
├──
[gui-apps](https://gitlab.com/ahrs/gentoo-stuff/tree/master/gui-apps/)  
│   └──
[wf-shell](https://gitlab.com/ahrs/gentoo-stuff/tree/master/gui-apps/wf-shell/)  
├──
[gui-libs](https://gitlab.com/ahrs/gentoo-stuff/tree/master/gui-libs/)  
│   └──
[wf-config](https://gitlab.com/ahrs/gentoo-stuff/tree/master/gui-libs/wf-config/)  
├── [gui-wm](https://gitlab.com/ahrs/gentoo-stuff/tree/master/gui-wm/)  
│   └──
[wayfire](https://gitlab.com/ahrs/gentoo-stuff/tree/master/gui-wm/wayfire/)  
├──
[kwin-scripts](https://gitlab.com/ahrs/gentoo-stuff/tree/master/kwin-scripts/)  
│   └──
[tiling](https://gitlab.com/ahrs/gentoo-stuff/tree/master/kwin-scripts/tiling/)  
├──
[mail-client](https://gitlab.com/ahrs/gentoo-stuff/tree/master/mail-client/)  
│   └──
[mailspring](https://gitlab.com/ahrs/gentoo-stuff/tree/master/mail-client/mailspring/)  
├──
[media-fonts](https://gitlab.com/ahrs/gentoo-stuff/tree/master/media-fonts/)  
│   ├──
[cherry](https://gitlab.com/ahrs/gentoo-stuff/tree/master/media-fonts/cherry/)  
│   ├──
[emojione-color-font](https://gitlab.com/ahrs/gentoo-stuff/tree/master/media-fonts/emojione-color-font/)  
│   ├──
[firacode](https://gitlab.com/ahrs/gentoo-stuff/tree/master/media-fonts/firacode/)  
│   ├──
[nerd-fonts](https://gitlab.com/ahrs/gentoo-stuff/tree/master/media-fonts/nerd-fonts/)  
│   └──
[scientifica](https://gitlab.com/ahrs/gentoo-stuff/tree/master/media-fonts/scientifica/)  
├──
[media-gfx](https://gitlab.com/ahrs/gentoo-stuff/tree/master/media-gfx/)  
│   ├──
[grim](https://gitlab.com/ahrs/gentoo-stuff/tree/master/media-gfx/grim/)  
│   └──
[mermaidcli](https://gitlab.com/ahrs/gentoo-stuff/tree/master/media-gfx/mermaidcli/)  
├──
[media-libs](https://gitlab.com/ahrs/gentoo-stuff/tree/master/media-libs/)  
│   ├──
[glfw](https://gitlab.com/ahrs/gentoo-stuff/tree/master/media-libs/glfw/)  
│   ├──
[libspotify](https://gitlab.com/ahrs/gentoo-stuff/tree/master/media-libs/libspotify/)  
│   └──
[mutagen](https://gitlab.com/ahrs/gentoo-stuff/tree/master/media-libs/mutagen/)  
├──
[media-sound](https://gitlab.com/ahrs/gentoo-stuff/tree/master/media-sound/)  
│   ├──
[cantata](https://gitlab.com/ahrs/gentoo-stuff/tree/master/media-sound/cantata/)  
│   ├──
[cava](https://gitlab.com/ahrs/gentoo-stuff/tree/master/media-sound/cava/)  
│   ├──
[glava](https://gitlab.com/ahrs/gentoo-stuff/tree/master/media-sound/glava/)  
│   ├──
[mpdris2](https://gitlab.com/ahrs/gentoo-stuff/tree/master/media-sound/mpdris2/)  
│   ├──
[pavucontrol-qt](https://gitlab.com/ahrs/gentoo-stuff/tree/master/media-sound/pavucontrol-qt/)  
│   ├──
[picard](https://gitlab.com/ahrs/gentoo-stuff/tree/master/media-sound/picard/)  
│   ├──
[pulsemixer](https://gitlab.com/ahrs/gentoo-stuff/tree/master/media-sound/pulsemixer/)  
│   ├──
[spotify](https://gitlab.com/ahrs/gentoo-stuff/tree/master/media-sound/spotify/)  
│   ├──
[tizonia](https://gitlab.com/ahrs/gentoo-stuff/tree/master/media-sound/tizonia/)  
│   └──
[wf-sound-control](https://gitlab.com/ahrs/gentoo-stuff/tree/master/media-sound/wf-sound-control/)  
├──
[media-tv](https://gitlab.com/ahrs/gentoo-stuff/tree/master/media-tv/)  
│   └──
[plex-media-server](https://gitlab.com/ahrs/gentoo-stuff/tree/master/media-tv/plex-media-server/)  
├──
[media-video](https://gitlab.com/ahrs/gentoo-stuff/tree/master/media-video/)  
│   ├──
[filebot](https://gitlab.com/ahrs/gentoo-stuff/tree/master/media-video/filebot/)  
│   ├──
[freetube](https://gitlab.com/ahrs/gentoo-stuff/tree/master/media-video/freetube/)  
│   ├──
[h265ize](https://gitlab.com/ahrs/gentoo-stuff/tree/master/media-video/h265ize/)  
│   ├──
[vobsub2srt](https://gitlab.com/ahrs/gentoo-stuff/tree/master/media-video/vobsub2srt/)  
│   └──
[wlstream](https://gitlab.com/ahrs/gentoo-stuff/tree/master/media-video/wlstream/)  
├──
[net-analyzer](https://gitlab.com/ahrs/gentoo-stuff/tree/master/net-analyzer/)  
│   └──
[SpeedTest++](https://gitlab.com/ahrs/gentoo-stuff/tree/master/net-analyzer/SpeedTest%2B%2B/)  
├──
[net-dns](https://gitlab.com/ahrs/gentoo-stuff/tree/master/net-dns/)  
│   └──
[pi-hole-ftl](https://gitlab.com/ahrs/gentoo-stuff/tree/master/net-dns/pi-hole-ftl/)  
├──
[net-libs](https://gitlab.com/ahrs/gentoo-stuff/tree/master/net-libs/)  
│   └──
[nodejs](https://gitlab.com/ahrs/gentoo-stuff/tree/master/net-libs/nodejs/)  
├──
[net-misc](https://gitlab.com/ahrs/gentoo-stuff/tree/master/net-misc/)  
│   └──
[youtube-dl](https://gitlab.com/ahrs/gentoo-stuff/tree/master/net-misc/youtube-dl/)  
├──
[net-p2p](https://gitlab.com/ahrs/gentoo-stuff/tree/master/net-p2p/)  
│   ├──
[jackett](https://gitlab.com/ahrs/gentoo-stuff/tree/master/net-p2p/jackett/)  
│   └──
[jackett-bin](https://gitlab.com/ahrs/gentoo-stuff/tree/master/net-p2p/jackett-bin/)  
├──
[net-wireless](https://gitlab.com/ahrs/gentoo-stuff/tree/master/net-wireless/)  
│   └──
[r8152](https://gitlab.com/ahrs/gentoo-stuff/tree/master/net-wireless/r8152/)  
├──
[sys-apps](https://gitlab.com/ahrs/gentoo-stuff/tree/master/sys-apps/)  
│   ├──
[exa](https://gitlab.com/ahrs/gentoo-stuff/tree/master/sys-apps/exa/)  
│   ├──
[firejail](https://gitlab.com/ahrs/gentoo-stuff/tree/master/sys-apps/firejail/)  
│   ├──
[firetools](https://gitlab.com/ahrs/gentoo-stuff/tree/master/sys-apps/firetools/)  
│   ├──
[hw-probe](https://gitlab.com/ahrs/gentoo-stuff/tree/master/sys-apps/hw-probe/)  
│   ├──
[openrc-shutdown-scripts](https://gitlab.com/ahrs/gentoo-stuff/tree/master/sys-apps/openrc-shutdown-scripts/)  
│   ├──
[pacman](https://gitlab.com/ahrs/gentoo-stuff/tree/master/sys-apps/pacman/)  
│   ├──
[sbase](https://gitlab.com/ahrs/gentoo-stuff/tree/master/sys-apps/sbase/)  
│   ├──
[sift](https://gitlab.com/ahrs/gentoo-stuff/tree/master/sys-apps/sift/)  
│   ├──
[topgrade](https://gitlab.com/ahrs/gentoo-stuff/tree/master/sys-apps/topgrade/)  
│   ├──
[ubase](https://gitlab.com/ahrs/gentoo-stuff/tree/master/sys-apps/ubase/)  
│   ├──
[uutils](https://gitlab.com/ahrs/gentoo-stuff/tree/master/sys-apps/uutils/)  
│   └──
[xbps](https://gitlab.com/ahrs/gentoo-stuff/tree/master/sys-apps/xbps/)  
├──
[sys-devel](https://gitlab.com/ahrs/gentoo-stuff/tree/master/sys-devel/)  
│   └──
[just](https://gitlab.com/ahrs/gentoo-stuff/tree/master/sys-devel/just/)  
├── [sys-fs](https://gitlab.com/ahrs/gentoo-stuff/tree/master/sys-fs/)  
│   ├──
[btrfs-du](https://gitlab.com/ahrs/gentoo-stuff/tree/master/sys-fs/btrfs-du/)  
│   ├──
[dutree](https://gitlab.com/ahrs/gentoo-stuff/tree/master/sys-fs/dutree/)  
│   └──
[rewritefs](https://gitlab.com/ahrs/gentoo-stuff/tree/master/sys-fs/rewritefs/)  
├──
[sys-process](https://gitlab.com/ahrs/gentoo-stuff/tree/master/sys-process/)  
│   ├──
[earlyoom](https://gitlab.com/ahrs/gentoo-stuff/tree/master/sys-process/earlyoom/)  
│   └──
[hegemon](https://gitlab.com/ahrs/gentoo-stuff/tree/master/sys-process/hegemon/)  
├──
[virtual](https://gitlab.com/ahrs/gentoo-stuff/tree/master/virtual/)  
│   └──
[electron](https://gitlab.com/ahrs/gentoo-stuff/tree/master/virtual/electron/)  
├──
[www-apps](https://gitlab.com/ahrs/gentoo-stuff/tree/master/www-apps/)  
│   ├──
[pi-hole-server](https://gitlab.com/ahrs/gentoo-stuff/tree/master/www-apps/pi-hole-server/)  
│   └──
[ympd](https://gitlab.com/ahrs/gentoo-stuff/tree/master/www-apps/ympd/)  
├──
[www-client](https://gitlab.com/ahrs/gentoo-stuff/tree/master/www-client/)  
│   └──
[browsh](https://gitlab.com/ahrs/gentoo-stuff/tree/master/www-client/browsh/)  
├──
[www-misc](https://gitlab.com/ahrs/gentoo-stuff/tree/master/www-misc/)  
│   └──
[buku](https://gitlab.com/ahrs/gentoo-stuff/tree/master/www-misc/buku/)  
├──
[www-servers](https://gitlab.com/ahrs/gentoo-stuff/tree/master/www-servers/)  
│   └──
[websocketd](https://gitlab.com/ahrs/gentoo-stuff/tree/master/www-servers/websocketd/)  
├──
[x11-base](https://gitlab.com/ahrs/gentoo-stuff/tree/master/x11-base/)  
│   └──
[xorg-server](https://gitlab.com/ahrs/gentoo-stuff/tree/master/x11-base/xorg-server/)  
├──
[x11-libs](https://gitlab.com/ahrs/gentoo-stuff/tree/master/x11-libs/)  
│   └──
[gtk+](https://gitlab.com/ahrs/gentoo-stuff/tree/master/x11-libs/gtk%2B/)  
├──
[x11-misc](https://gitlab.com/ahrs/gentoo-stuff/tree/master/x11-misc/)  
│   ├──
[brightnessctl](https://gitlab.com/ahrs/gentoo-stuff/tree/master/x11-misc/brightnessctl/)  
│   ├──
[colorpicker](https://gitlab.com/ahrs/gentoo-stuff/tree/master/x11-misc/colorpicker/)  
│   ├──
[dunst](https://gitlab.com/ahrs/gentoo-stuff/tree/master/x11-misc/dunst/)  
│   ├──
[i3status-rust](https://gitlab.com/ahrs/gentoo-stuff/tree/master/x11-misc/i3status-rust/)  
│   ├──
[lemonbar](https://gitlab.com/ahrs/gentoo-stuff/tree/master/x11-misc/lemonbar/)  
│   ├──
[redshift](https://gitlab.com/ahrs/gentoo-stuff/tree/master/x11-misc/redshift/)  
│   ├──
[rofi-top](https://gitlab.com/ahrs/gentoo-stuff/tree/master/x11-misc/rofi-top/)  
│   ├──
[wmutils](https://gitlab.com/ahrs/gentoo-stuff/tree/master/x11-misc/wmutils/)  
│   └──
[xsecurelock](https://gitlab.com/ahrs/gentoo-stuff/tree/master/x11-misc/xsecurelock/)  
├──
[x11-terms](https://gitlab.com/ahrs/gentoo-stuff/tree/master/x11-terms/)  
│   ├──
[alacritty](https://gitlab.com/ahrs/gentoo-stuff/tree/master/x11-terms/alacritty/)  
│   ├──
[galacritty](https://gitlab.com/ahrs/gentoo-stuff/tree/master/x11-terms/galacritty/)  
│   ├──
[hyper](https://gitlab.com/ahrs/gentoo-stuff/tree/master/x11-terms/hyper/)  
│   └──
[kitty](https://gitlab.com/ahrs/gentoo-stuff/tree/master/x11-terms/kitty/)  
├──
[x11-themes](https://gitlab.com/ahrs/gentoo-stuff/tree/master/x11-themes/)  
│   └──
[SierraBreeze](https://gitlab.com/ahrs/gentoo-stuff/tree/master/x11-themes/SierraBreeze/)  
└── [x11-wm](https://gitlab.com/ahrs/gentoo-stuff/tree/master/x11-wm/)  
    ├──
[custard](https://gitlab.com/ahrs/gentoo-stuff/tree/master/x11-wm/custard/)  
    └──
[windowchef](https://gitlab.com/ahrs/gentoo-stuff/tree/master/x11-wm/windowchef/)  
  
  

  
  

------------------------------------------------------------------------

tree v1.7.0 © 1996 - 2014 by Steve Baker and Thomas Moore  
HTML output hacked and copyleft © 1998 by Francesc Rocher  
JSON output hacked and copyleft © 2014 by Florian Sesser  
Charsets / OS/2 support © 2001 by Kyosuke Tokoro

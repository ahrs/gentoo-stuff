# Copyright 2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Video capture for Wayland"
HOMEPAGE="https://github.com/atomnuker/wlstream"

inherit git-r3 meson
EGIT_REPO_URI="${HOMEPAGE}.git"

LICENSE="LGPL-2"
SLOT="0"
KEYWORDS=""
IUSE=""

PATCHES="
	${FILESDIR}/9.diff
"

DEPEND="
	>=media-video/ffmpeg-4.0[libdrm]
	media-sound/pulseaudio
	dev-libs/wayland
	dev-libs/wayland-protocols
"
RDEPEND="
	>=media-video/ffmpeg-4.0[libdrm]
	media-sound/pulseaudio
"
BDEPEND=""

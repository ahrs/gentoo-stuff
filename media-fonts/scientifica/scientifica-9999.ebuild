# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="5"

inherit git-r3

DESCRIPTION="Tall, condensed, bitmap font for geeks."
HOMEPAGE="https://github.com/NerdyPepper/scientifica"
#SRC_URI=""
EGIT_REPO_URI="${HOMEPAGE}.git"
EGIT_BRANCH="master"

LICENSE="OFL-1.1"
SLOT="0"
KEYWORDS=""

IUSE="-hidpi"

DEPEND="
	hidpi? ( dev-python/virtualenv )
"

S="${WORKDIR}"

src_compile() {
	if ! use hidpi
	then
		return 0;
	fi

	cd "$PF" || die
	virtualenv --python=python2 ./.virtualenv
	. ./.virtualenv/bin/activate
	pip install bdflib
	python2 doubler.py < regular/scientifica-11.bdf > regular/scientifica-11_2x.bdf
	mv regular/scientifica-11_2x.bdf regular/scientifica-11.bdf
	python2 doubler.py < bold/scientificaBold-11.bdf > bold/scientificaBold-11_2x.bdf
	mv bold/scientificaBold-11_2x.bdf bold/scientificaBold-11.bdf
}

src_install() {
	cd "$PF" || die
	install -Dm644 "${FILESDIR}"/75-yes-${PN}.conf "${ED}/etc/fonts/conf.avail/75-yes-${PN}.conf"
	install -Dm644 regular/${PN}-11.bdf "${ED}/usr/share/fonts/misc/${PN}-11.bdf"
	install -Dm644 bold/${PN}Bold-11.bdf "${ED}/usr/share/fonts/misc/${PN}Bold-11.bdf"
	install -dm755 "${ED}/etc/fonts/conf.d/"
	install -dm755 "${ED}/usr/share/fonts/"
	ln -sf /etc/fonts/conf.avail/70-yes-bitmaps.conf "${ED}/etc/fonts/conf.d/"
}

# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="5"

inherit font

DESCRIPTION="Iconic font aggregator, collection, and patcher"
HOMEPAGE="https://nerdfonts.com/"
SRC_URI="
	"https://github.com/ryanoasis/nerd-fonts/releases/download/v${PV}/3270.zip" -> "${P}_3270.zip"
	"https://github.com/ryanoasis/nerd-fonts/releases/download/v${PV}/AnonymousPro.zip" -> "${P}_AnonymousPro.zip"
	"https://github.com/ryanoasis/nerd-fonts/releases/download/v${PV}/Arimo.zip" -> "${P}_Arimo.zip"
	"https://github.com/ryanoasis/nerd-fonts/releases/download/v${PV}/AurulentSansMono.zip" -> "${P}_AurulentSansMono.zip"
	"https://github.com/ryanoasis/nerd-fonts/releases/download/v${PV}/BitstreamVeraSansMono.zip" -> "${P}_BitstreamVeraSansMono.zip"
	"https://github.com/ryanoasis/nerd-fonts/releases/download/v${PV}/CodeNewRoman.zip" -> "${P}_CodeNewRoman.zip"
	"https://github.com/ryanoasis/nerd-fonts/releases/download/v${PV}/Cousine.zip" -> "${P}_Cousine.zip"
	"https://github.com/ryanoasis/nerd-fonts/releases/download/v${PV}/DejaVuSansMono.zip" -> "${P}_DejaVuSansMono.zip"
	"https://github.com/ryanoasis/nerd-fonts/releases/download/v${PV}/DroidSansMono.zip" -> "${P}_DroidSansMono.zip"
	"https://github.com/ryanoasis/nerd-fonts/releases/download/v${PV}/FantasqueSansMono.zip" -> "${P}_FantasqueSansMono.zip"
	"https://github.com/ryanoasis/nerd-fonts/releases/download/v${PV}/FiraCode.zip" -> "${P}_FiraCode.zip"
	"https://github.com/ryanoasis/nerd-fonts/releases/download/v${PV}/FiraMono.zip" -> "${P}_FiraMono.zip"
	"https://github.com/ryanoasis/nerd-fonts/releases/download/v${PV}/Go-Mono.zip" -> "${P}_Go-Mono.zip"
	"https://github.com/ryanoasis/nerd-fonts/releases/download/v${PV}/Gohu.zip" -> "${P}_Gohu.zip"
	"https://github.com/ryanoasis/nerd-fonts/releases/download/v${PV}/Hack.zip" -> "${P}_Hack.zip"
	"https://github.com/ryanoasis/nerd-fonts/releases/download/v${PV}/Hasklig.zip" -> "${P}_Hasklig.zip"
	"https://github.com/ryanoasis/nerd-fonts/releases/download/v${PV}/HeavyData.zip" -> "${P}_HeavyData.zip"
	"https://github.com/ryanoasis/nerd-fonts/releases/download/v${PV}/Hermit.zip" -> "${P}_Hermit.zip"
	"https://github.com/ryanoasis/nerd-fonts/releases/download/v${PV}/Inconsolata.zip" -> "${P}_Inconsolata.zip"
	"https://github.com/ryanoasis/nerd-fonts/releases/download/v${PV}/InconsolataGo.zip" -> "${P}_InconsolataGo.zip"
	"https://github.com/ryanoasis/nerd-fonts/releases/download/v${PV}/InconsolataLGC.zip" -> "${P}_InconsolataLGC.zip"
	"https://github.com/ryanoasis/nerd-fonts/releases/download/v${PV}/Iosevka.zip" -> "${P}_Iosevka.zip"
	"https://github.com/ryanoasis/nerd-fonts/releases/download/v${PV}/Lekton.zip" -> "${P}_Lekton.zip"
	"https://github.com/ryanoasis/nerd-fonts/releases/download/v${PV}/LiberationMono.zip" -> "${P}_LiberationMono.zip"
	"https://github.com/ryanoasis/nerd-fonts/releases/download/v${PV}/Meslo.zip" -> "${P}_Meslo.zip"
	"https://github.com/ryanoasis/nerd-fonts/releases/download/v${PV}/Monofur.zip" -> "${P}_Monofur.zip"
	"https://github.com/ryanoasis/nerd-fonts/releases/download/v${PV}/Monoid.zip" -> "${P}_Monoid.zip"
	"https://github.com/ryanoasis/nerd-fonts/releases/download/v${PV}/Mononoki.zip" -> "${P}_Mononoki.zip"
	"https://github.com/ryanoasis/nerd-fonts/releases/download/v${PV}/MPlus.zip" -> "${P}_MPlus.zip"
	"https://github.com/ryanoasis/nerd-fonts/releases/download/v${PV}/ProFont.zip" -> "${P}_ProFont.zip"
	"https://github.com/ryanoasis/nerd-fonts/releases/download/v${PV}/ProggyClean.zip" -> "${P}_ProggyClean.zip"
	"https://github.com/ryanoasis/nerd-fonts/releases/download/v${PV}/RobotoMono.zip" -> "${P}_RobotoMono.zip"
	"https://github.com/ryanoasis/nerd-fonts/releases/download/v${PV}/ShareTechMono.zip" -> "${P}_ShareTechMono.zip"
	"https://github.com/ryanoasis/nerd-fonts/releases/download/v${PV}/SourceCodePro.zip" -> "${P}_SourceCodePro.zip"
	"https://github.com/ryanoasis/nerd-fonts/releases/download/v${PV}/SpaceMono.zip" -> "${P}_SpaceMono.zip"
	"https://github.com/ryanoasis/nerd-fonts/releases/download/v${PV}/Terminus.zip" -> "${P}_Terminus.zip"
	"https://github.com/ryanoasis/nerd-fonts/releases/download/v${PV}/Tinos.zip" -> "${P}_Tinos.zip"
	"https://github.com/ryanoasis/nerd-fonts/releases/download/v${PV}/Ubuntu.zip" -> "${P}_Ubuntu.zip"
	"https://github.com/ryanoasis/nerd-fonts/releases/download/v${PV}/UbuntuMono.zip" -> "${P}_UbuntuMono.zip"
"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~*"

S="${WORKDIR}"

FONT_S="${S}"
FONT_SUFFIX="ttf"

src_unpack() {
	#cp "${DISTDIR}"/*.ttf "${WORKDIR}"
	for f in "${DISTDIR}"/*.zip
	do
		7z x "$f" -o"${WORKDIR}"
	done
}

src_install() {
	insinto /usr/share/fonts/${PN}

	cd "${WORKDIR}"
	doins *.ttf

	font_xfont_config
	font_fontconfig
}

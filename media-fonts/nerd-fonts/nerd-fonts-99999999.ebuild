# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="6"

inherit font git-r3 multiprocessing

DESCRIPTION="Iconic font aggregator, collection, and patcher"
HOMEPAGE="https://nerdfonts.com/"
EGIT_REPO_URI="https://github.com/ryanoasis/nerd-fonts"
EGIT_CLONE_TYPE="shallow"

LICENSE="MIT"
SLOT="0"
KEYWORDS=""

# grep -oE 'name=".*"' metadata.xml | sed -e 's|^name="||g' -e 's|"$||g'
IUSE="-all-fonts -generate-manifest +scripts +3270 +agave +anonymouspro +arimo +aurulentsansmono +bigblueterminal +bitstreamverasansmono +blex +codenewroman +cousine +daddytimemono +dejavusansmono +droidsansmono +fantasquesansmono +firacode +firamono +go-mono +gohu +hack +hasklig +heavydata +hermit +inconsolata +inconsolatago +inconsolatalgc +iosevka +lekton +liberationmono +mplus +meslo +monofur +monoid +mononoki +noto +opendyslexic +overpass +profont +proggyclean +robotomono +sharetechmono +sourcecodepro +spacemono +terminus +tinos +ubuntu +ubuntumono +victormono"

S="${WORKDIR}/${P}"

FONT_S="${S}"
FONT_SUFFIX="ttf"

_generate_manifest() {
	cd "${S}/patched-fonts" || die
	cat > "${T}"/metadata.xml <<EOF
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE pkgmetadata SYSTEM "http://www.gentoo.org/dtd/metadata.dtd">
<pkgmetadata>
<maintainer type="person">
<email>mail@ahrs.me</email>
</maintainer>
<longdescription lang="en">
</longdescription>
<use>
<flag name="all-fonts">Use this if you don't care about filtering which fonts to install and want every nerd-font</flag>
<flag name="generate-manifest">Internal flag to generate the ebuilds manifest (this is for use by the package maintainer only, don't set this)</flag>
<flag name="scripts">Install extra scripts to /usr/share/nerd-fonts</flag>
$(for d in ./*/; do _d="${d/.\//}"; _d="${_d/\/}"; echo "<flag name=\"${_d,,}\">$_d</flag>"; done;)
</use>
</pkgmetadata>
EOF
	die "You have enabled the generate-manifest useflag. This is for internal use only and should *not* be set directly by users"
}

src_prepare() {
	use generate-manifest && {
		_generate_manifest
		die
	}
	default
}

src_install() {
	insinto /usr/share/fonts/${PN}

	cd "${S}/patched-fonts" || die

	makeopts_jobs="$(makeopts_jobs)"
	is_windows=false
	use elibc_Cygwin || use elibc_Winnt && is_windows=true
	for d in ./*/
	do
		_d="${d/.\//}"
		_d="${_d/\/}"
		if ! use "${_d,,}" && ! use all-fonts
		then
			continue
		fi
		jobs=0
		find "$d" -type f -name "*.ttf" -or -name "*.TTF" -or -name "*.otf" | while read -r line
		do
			[[ -z $line ]] && continue
			# Skip the Windows compatible fonts unless we're using
			# Gentoo Prefix on Windows ;)
			[[ $line == *Windows\ Compatible* && $is_windows == false ]] && continue
			doins "$line" &
			jobs=$((jobs+1))
			[[ $jobs == $makeopts_jobs ]] && {
				wait
				jobs=0
			}
		done
	done

	use scripts && {
		dodir '/usr/share/nerd-fonts'
		cp "${S}/bin/scripts/lib/"*.sh "${ED}/usr/share/nerd-fonts"
	}

	font_xfont_config
	font_fontconfig
}

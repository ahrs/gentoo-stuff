# Copyright 2017-{{ eval( printf "%(%Y)T" ) }} Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

# extracted from:
# curl -s https://raw.githubusercontent.com/redox-os/ion/master/Cargo.lock | grep -oE '^"checksum\s.*"' | awk '{print $2 "-" $3}' | xclip -selection clipboard -i
# NOTE: Some deps are fetched directly from git so need to be removed from this list
CRATES="
{{ eval(curl -s https://raw.githubusercontent.com/redox-os/ion/master/Cargo.lock | grep -oE '^"checksum\s.*"' | awk '{print $2 "-" $3}' | grep -v -E "liner-0.4.5|calculate-0.7.0" ) }}"

inherit cargo git-r3

DESCRIPTION="The Ion Shell. Compatible with Redox and Linux."
HOMEPAGE="https://gitlab.redox-os.org/redox-os/ion"
SRC_URI="$(cargo_crate_uris ${CRATES})"

EGIT_REPO_URI="
	https://gitlab.redox-os.org/redox-os/${PN}.git
	https://github.com/redox-os/${PN}.git
"

LICENSE="MIT"
SLOT="0"
KEYWORDS=""
IUSE="+examples fetch-crates +man sccache test"

BDEPEND="
	sccache? ( dev-util/sccache )
	man? ( app-text/mdbook )
	>=virtual/rust-1.30.0
"

src_unpack() {
	git-r3_src_unpack
	cd "${WORKDIR}/${P}" || die

	cargo_src_unpack

	export CARGO_HOME="${ECARGO_HOME}"

	# fetch git-dependencies. network-sandbox isn't active here so we can avoid using RESTRICT="network-sandbox"
	find . -name "Cargo.toml" -exec cargo fetch --manifest-path {} \;
}

src_compile() {
	use sccache && export RUSTC_WRAPPER=sccache
	cargo_src_compile
	use examples && {
		cd manual || die
		mdbook build || die
	}
}

src_install() {
	# don't compress examples
	docompress -x /usr/share/doc/${PF}/examples

	# install the shell into /bin instead of /usr/bin
	# this is consistent with the other shells in the app-shells category
	into /
	dobin target/release/${PN}
	use examples && dodoc -r examples
	use man && dodoc -r manual/book
}

src_test() {
	sed -i \
		-e 's| +$(TOOLCHAIN)||g' \
		-e 's|TOOLCHAIN=$(TOOLCHAIN)||g' Makefile
	sed -i \
		-e 's| +$TOOLCHAIN||g' \
		-e 's| -z "$TOOLCHAIN"| -z "${TOOLCHAIN:-}"|g' examples/run_examples.sh
	emake tests
}

pkg_postinst() {
	ebegin "Updating /etc/shells"
	( grep -v "^/bin/ion$" /etc/shells; echo "/bin/ion" ) > "${T}"/shells
	mv -f "${T}"/shells /etc/shells
	eend $?
}

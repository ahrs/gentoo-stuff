# Copyright 2017-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

# extracted from:
# curl -s https://raw.githubusercontent.com/redox-os/ion/master/Cargo.lock | grep -oE '^"checksum\s.*"' | awk '{print $2 "-" $3}' | xclip -selection clipboard -i
# NOTE: Some deps are fetched directly from git so need to be removed from this list
CRATES="
aho-corasick-0.7.3
ansi_term-0.11.0
arrayvec-0.4.10
atty-0.2.11
auto_enums-0.5.5
auto_enums_core-0.5.5
auto_enums_derive-0.5.5
autocfg-0.1.2
backtrace-0.3.15
backtrace-sys-0.1.28
bitflags-1.0.4
bytecount-0.3.2
byteorder-1.3.1
cast-0.2.2
cc-1.0.35
cfg-if-0.1.7
clap-2.33.0
cloudabi-0.0.3
criterion-0.2.11
criterion-plot-0.3.1
crossbeam-deque-0.2.0
crossbeam-epoch-0.3.1
crossbeam-utils-0.2.2
csv-1.0.6
csv-core-0.1.5
decimal-2.0.4
derive_utils-0.7.0
either-1.5.2
err-derive-0.1.5
errno-dragonfly-0.1.1
failure-0.1.5
failure_derive-0.1.5
fuchsia-cprng-0.1.1
gcc-0.3.55
glob-0.3.0
hashbrown-0.1.8
itertools-0.8.0
itoa-0.4.3
lazy_static-1.3.0
lexical-2.1.0
lexical-core-0.4.0
libc-0.2.51
lock_api-0.1.5
memchr-2.2.0
memoffset-0.2.1
nodrop-0.1.13
num-0.1.42
num-bigint-0.1.44
num-complex-0.1.43
num-integer-0.1.39
num-iter-0.1.37
num-rational-0.1.42
num-traits-0.2.6
num_cpus-1.10.0
numtoa-0.1.0
object-pool-0.3.1
ord_subset-3.1.1
owning_ref-0.4.0
parking_lot-0.7.1
parking_lot_core-0.4.0
permutate-0.3.2
proc-macro2-0.4.27
quote-0.6.12
rand-0.4.6
rand-0.6.5
rand_chacha-0.1.1
rand_core-0.3.1
rand_core-0.4.0
rand_hc-0.1.0
rand_isaac-0.1.1
rand_jitter-0.1.3
rand_os-0.1.3
rand_pcg-0.1.2
rand_xorshift-0.1.1
rand_xoshiro-0.1.0
rayon-1.0.3
rayon-core-1.4.1
rdrand-0.4.0
redox_syscall-0.1.54
redox_termios-0.1.1
regex-1.1.5
regex-syntax-0.6.6
rustc-demangle-0.1.13
rustc-serialize-0.3.24
rustc_version-0.2.3
ryu-0.2.7
same-file-1.0.4
scopeguard-0.3.3
semver-0.9.0
semver-parser-0.7.0
serde-1.0.90
serde_derive-1.0.90
serde_json-1.0.39
serial_test-0.2.0
serial_test_derive-0.2.0
small-0.1.0
smallvec-0.6.9
stable_deref_trait-1.1.1
stackvector-1.0.2
static_assertions-0.2.5
strsim-0.8.0
syn-0.15.30
synstructure-0.10.1
termion-1.5.1
textwrap-0.11.0
thread_local-0.3.6
tinytemplate-1.0.1
ucd-util-0.1.3
unicode-segmentation-1.2.1
unicode-width-0.1.5
unicode-xid-0.1.0
unreachable-1.0.0
users-0.8.1
utf8-ranges-1.0.2
vec_map-0.8.1
version_check-0.1.5
void-1.0.2
walkdir-2.2.7
winapi-0.3.7
winapi-i686-pc-windows-gnu-0.4.0
winapi-util-0.1.2
winapi-x86_64-pc-windows-gnu-0.4.0
xdg-2.2.0
"

inherit cargo git-r3

DESCRIPTION="The Ion Shell. Compatible with Redox and Linux."
HOMEPAGE="https://gitlab.redox-os.org/redox-os/ion"
SRC_URI="$(cargo_crate_uris ${CRATES})"

EGIT_REPO_URI="
	https://gitlab.redox-os.org/redox-os/${PN}.git
	https://github.com/redox-os/${PN}.git
"

LICENSE="MIT"
SLOT="0"
KEYWORDS=""
IUSE="+examples fetch-crates +man sccache test"

BDEPEND="
	sccache? ( dev-util/sccache )
	man? ( app-text/mdbook )
	>=virtual/rust-1.30.0
"

src_unpack() {
	git-r3_src_unpack
	cd "${WORKDIR}/${P}" || die

	cargo_src_unpack

	export CARGO_HOME="${ECARGO_HOME}"

	# fetch git-dependencies. network-sandbox isn't active here so we can avoid using RESTRICT="network-sandbox"
	find . -name "Cargo.toml" -exec cargo fetch --manifest-path {} \;
}

src_compile() {
	use sccache && export RUSTC_WRAPPER=sccache
	cargo_src_compile
	use examples && {
		cd manual || die
		mdbook build || die
	}
}

src_install() {
	# don't compress examples
	docompress -x /usr/share/doc/${PF}/examples

	# install the shell into /bin instead of /usr/bin
	# this is consistent with the other shells in the app-shells category
	into /
	dobin target/release/${PN}
	use examples && dodoc -r examples
	use man && dodoc -r manual/book
}

src_test() {
	sed -i \
		-e 's| +$(TOOLCHAIN)||g' \
		-e 's|TOOLCHAIN=$(TOOLCHAIN)||g' Makefile
	sed -i \
		-e 's| +$TOOLCHAIN||g' \
		-e 's| -z "$TOOLCHAIN"| -z "${TOOLCHAIN:-}"|g' examples/run_examples.sh
	emake tests
}

pkg_postinst() {
	ebegin "Updating /etc/shells"
	( grep -v "^/bin/ion$" /etc/shells; echo "/bin/ion" ) > "${T}"/shells
	mv -f "${T}"/shells /etc/shells
	eend $?
}

# Copyright 2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="MPRIS plugin for mpv"
HOMEPAGE="https://github.com/hoyon/mpv-mpris"

if [[ -z ${PV%%*9999} ]]; then
	inherit git-r3
	EGIT_REPO_URI="${HOMEPAGE}.git"
else
	SRC_URI="${HOMEPAGE}/archive/${PV}.tar.gz -> ${P}.tar.gz"
	RESTRICT="primaryuri"
	KEYWORDS="~amd64 ~x86"
fi

LICENSE="MIT"
SLOT="0"
IUSE="+autoload"

DEPEND="dev-libs/glib"
RDEPEND="
	${DEPEND}
	media-video/mpv
"
BDEPEND=""

inherit toolchain-funcs

src_install() {
	local libdir="$(get_libdir)/mpv"
	make install || die "\`make install\` failed"

	dodir /etc/mpv/scripts
	dodir "$libdir"
	exeinto "$libdir"
	doexe "${HOME}/.config/mpv/scripts/mpris.so"
	use autoload && ln -sv "/$libdir/mpris.so" "${ED}/etc/mpv/scripts"
}

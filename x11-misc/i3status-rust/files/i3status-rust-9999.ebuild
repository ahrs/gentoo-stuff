# Copyright 2017-{{ eval( printf "%(%Y)T" ) }} Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

CRATES="
{{ eval(curl -s https://raw.githubusercontent.com/greshake/i3status-rust/master/Cargo.lock | grep -oE '^"checksum\s.*"' | awk '{print $2 "-" $3}') }}"

inherit cargo fcaps git-r3 multiprocessing

DESCRIPTION="resourcefriendly and feature-rich replacement for i3status, written in pure Rust"
HOMEPAGE="https://github.com/greshake/i3status-rust"
SRC_URI="$(cargo_crate_uris ${CRATES})"
EGIT_REPO_URI="${HOMEPAGE}.git"
RESTRICT="mirror"
LICENSE="CC0-1.0"
SLOT="0"
KEYWORDS=""
IUSE="debug sccache"

BDEPEND="
	sccache? ( dev-util/sccache )
"
DEPEND=""
RDEPEND="${DEPEND}"

src_unpack() {
	cargo_src_unpack
	git-r3_src_unpack
}

src_compile() {
	use sccache && export RUSTC_WRAPPER=sccache
	cargo_src_compile
}

src_install() {
	cargo install -j $(makeopts_jobs) --root="${D}/usr" --path . $(usex debug --debug "") || die "cargo install failed"
	[ -f "${D}/usr/.crates.toml" ] && rm -f "${D}/usr/.crates.toml"
	dodoc example_*.toml
}


pkg_postinst() {
	fcaps cap_net_admin usr/bin/i3status-rs
	einfo "${PN} can be used with any of the following programs:"
	einfo "   i3bar (x11-wm/i3|x11-wm/i3-gaps)"
	einfo "   swaybar (gui-wm/sway)"
	einfo "   x11-misc/xmobar"
	einfo "   x11-misc/dzen"
	einfo "${PN} can be used with the following additional software:"
	einfo "   media-sound/alsa-utils: For the volume block"
	einfo "   net-misc/curl: For the weather block"
	einfo "   sys-apps/lm_sensors: For the temperature block"
	einfo "   net-misc/networkmanager: For the networkmanager block"
	einfo "   media-fonts/powerline-symbols: For all themes using the powerline arrow char"
	einfo "   media-sound/pulseaudio: For the volume block"
	einfo "   net-analyzer/speedtest-cli: For the speedtest block"
	einfo "   media-fonts/fontawesome: For the awesome icons"
	einfo "   sys-power/upower: For the battery block"
	einfo "For more information, please refer to the documentation available at: https://github.com/greshake/i3status-rust/wiki"
}

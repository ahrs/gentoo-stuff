# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit autotools

if [[ -z ${PV%%*9999} ]]; then
	inherit git-r3
	EGIT_REPO_URI="https://github.com/google/${PN}"
	EGIT_BOOTSTRAP=""
	KEYWORDS=""
else
	SRC_URI="https://github.com/google/${PN}/archive/${PV}.tar.gz -> ${P}.tar.gz"
	KEYWORDS="~amd64 ~x86"
fi

DESCRIPTION="X11 screen lock utility with security in mind"
HOMEPAGE="https://github.com/google/xsecurelock"

IUSE="apache mplayer mpv pam xscreensaver"

SLOT="0"
LICENSE="Apache-2.0"
RDEPEND="
	apache? ( app-admin/apache-tools )
	mplayer? ( media-video/mplayer )
	mpv? ( media-video/mpv )
	pam? ( virtual/pam )
	xscreensaver? ( x11-misc/xscreensaver )
	x11-libs/libX11
	x11-libs/libXScrnSaver
"
DEPEND="${RDEPEND}"
BDEPEND=""

src_prepare() {
	eautoreconf
	default
}

src_configure() {
	local myeconfargs=(
			--with-pam-service-name=system-auth
			--prefix=/usr
	)
	./configure ${myeconfargs[@]} || die
}

src_install() {
	make DESTDIR="${ED}" install || die

	mv "${ED}/usr/share/doc/${PN}" "${ED}/usr/share/doc/${P}" || die
}

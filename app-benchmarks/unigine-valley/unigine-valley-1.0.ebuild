# Copyright 2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Latest Interactive Unigine Benchmark"
HOMEPAGE="http://www.unigine.com"

_PN=Unigine_Valley

SRC_URI="https://assets.unigine.com/d/${_PN}-${PV}.run -> ${P}.run"

LICENSE="UNIGINE_Engine"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="
	virtual/opengl
	x11-libs/libXrandr
	x11-libs/libXinerama
	media-libs/fontconfig
"
RDEPEND="${DEPEND}"
BDEPEND=""

src_unpack() {
	cd "${WORKDIR}"
	sh "${DISTDIR}/${P}".run --target "${P}" --noexec
}

src_prepare() {
	# The "bash" script is practically valid POSIX sh
	sed -i \
		-e 's|#!/bin/bash|#!/bin/sh -e|g' \
		-e 's|==|=|g' valley
	sed -i "s|cd ./bin|cd /opt/${P}/bin|g" valley

	default
}

src_install() {
	dodir /opt/bin
	insinto /opt
	doins -r "../${P}"
	ln -s /opt/"${P}"/valley "${D}/opt/bin/valley"

	local executables=(
		bin/libQtNetworkUnigine_x64.so.4
		bin/browser_x86
		bin/libQtWebKitUnigine_x64.so.4
		bin/libQtCoreUnigine_x64.so.4
		bin/libAppStereo_x64.so
		bin/libQtGuiUnigine_x86.so.4
		bin/browser_x64
		bin/libQtWebKitUnigine_x86.so.4
		bin/libAppStereo_x86.so
		bin/libUnigine_x86.so
		bin/x86/libopenal.so
		bin/libQtCoreUnigine_x86.so.4
		bin/libAppWall_x86.so
		bin/libQtNetworkUnigine_x86.so.4
		bin/libAppSurround_x86.so
		bin/valley_x86
		bin/libQtGuiUnigine_x64.so.4
		bin/libUnigine_x64.so
		bin/libAppWall_x64.so
		bin/libAppSurround_x64.so
		bin/libQtXmlUnigine_x86.so.4
		bin/libGPUMonitor_x86.so
		bin/libQtXmlUnigine_x64.so.4
		bin/x64/libopenal.so
		bin/valley_x64
		bin/libGPUMonitor_x64.so
		valley
	)

	for executable in ${executables[@]}
	do
		chmod +x "${D}/opt/${P}/${executable}" || die "Unable to set executable bit on ${D}/opt/${P}/${executable}"
	done
}

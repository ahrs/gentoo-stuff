# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

if [[ -z ${PV%%*9999} ]]; then
	inherit git-r3
	EGIT_REPO_URI="https://github.com/mermaidjs/mermaid.cli.git"
else
	SRC_URI="
		$SRC_URI
		mirror://githubcl/mermaidjs/mermaid.cli/tar.gz/v${PV} -> ${P}.tar.gz
	"
	RESTRICT="primaryuri"
	KEYWORDS="~amd64 ~x86"
fi

inherit multilib

RESTRICT="${RESTRICT} mirror network-sandbox"

DESCRIPTION="Command-line interface for mermaid"
HOMEPAGE="https://github.com/mermaidjs/mermaid.cli"

LICENSE="MIT"
SLOT="0"
IUSE=""

DEPEND="
	net-libs/nodejs[npm]
	>=sys-apps/yarn-1.12.3
"
RDEPEND="${DEPEND}"
DOCS=( README.md )

PATCHES=(
	"${FILESDIR}/${PN}-${PV}_use_system_chromium.diff"
)

src_compile() {
	export NODE_ENV=production
	export HOME="${T}"
	# Use system chrome
	export PUPPETEER_SKIP_CHROMIUM_DOWNLOAD=true
	yarn --no-default-rc --production --verbose --cache-folder ../cache || die
	mv node_modules ../node_modules
	yarn --no-default-rc --production=false --verbose --cache-folder ../cache || die
}

src_install() {
	export NODE_ENV="production"
	export HOME="${T}"
	yarn pack --no-default-rc --production --offline --cache-folder ../cache --filename ../"${P}.tar.gz"
	libdir="$(get_libdir)"
	dodir "/usr/$libdir/node_modules/mermaid.cli"

	source copy_modules.sh

	mv ../node_modules "${ED}/usr/$libdir/node_modules/mermaid.cli" || die
	mv fontawesome "${ED}/usr/$libdir/node_modules/mermaid.cli" || die

	tar -zxf ../"${P}.tar.gz" -C "${ED}/usr/$libdir/node_modules/mermaid.cli" || die
	pushd "${ED}/usr/$libdir/node_modules/mermaid.cli" || die
	mv package/* . || die
	# This emulates GNU tar's --strip-components=1
	pushd package || die
	for f in .*
	do
		if [ "$f" = "." ] || [ "$f" = ".." ]
		then
			continue
		fi
		mv "$f" .. || die
	done
	popd || die
	rm -rf package || die
	popd || die

	chmod +x index.js
	cp -f index.js mermaid.min.js LICENSE "${ED}/usr/$libdir/node_modules/mermaid.cli/" || die
	dosym "/usr/$libdir/node_modules/mermaid.cli/index.js" /usr/bin/mmdc

	einstalldocs

	dodoc "${D}/usr/$(get_libdir)/node_modules/${PN/cli/.cli}/LICENSE"
}

# Copyright 2018 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Grab images from a Wayland compositor"
HOMEPAGE="https://wayland.emersion.fr/grim"

inherit git-r3 meson

EGIT_REPO_URI="https://github.com/emersion/grim.git"

LICENSE="MIT"
SLOT="0"
KEYWORDS=""
IUSE=""

DEPEND="
	>=dev-libs/wayland-1.16.0
	>=dev-libs/wayland-protocols-1.16
	>=x11-libs/cairo-1.16.0-r2
"
RDEPEND="${DEPEND}"
BDEPEND=""

src_install() {
	dodoc "README.md"

	meson_src_install
}

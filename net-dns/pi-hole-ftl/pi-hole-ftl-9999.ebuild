# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

DESCRIPTION="The Pi-hole FTL engine"
HOMEPAGE="https://github.com/pi-hole/FTL"

__FTL_MAN_VERSION__="4.0"
__FTL_PATCH_VERSION__="4.0"

SRC_URI="
	man? ( https://raw.githubusercontent.com/pi-hole/pi-hole/v${__FTL_MAN_VERSION__}/manpages/pihole-FTL.8 -> ${PN}-${__FTL_MAN_VERSION__}_pihole-FTL.8 )
	man? ( https://raw.githubusercontent.com/pi-hole/pi-hole/v${__FTL_MAN_VERSION__}/manpages/pihole-FTL.conf.5 -> ${PN}-${__FTL_MAN_VERSION__}_pihole-FTL.conf.5 )
"

inherit eutils git-r3 readme.gentoo-r1 systemd user

EGIT_REPO_URI="https://github.com/pi-hole/FTL.git"
EGIT_BRANCH="${EGIT_BRANCH:-development}"

RESTRICT="mirror test? ( network-sandbox )"

LICENSE="EUPL-1.1"
SLOT="0"
KEYWORDS=""
IUSE="+caps +man systemd test"

DEPEND="
	caps? ( sys-libs/libcap )
	dev-db/sqlite
"
RDEPEND="${DEPEND}"

#S="${WORKDIR}/FTL-${PV}"

# shoutout to the AUR maintainer: https://aur.archlinux.org/packages/pi-hole-ftl/
src_prepare() {
	local _ssc="$T/sedcontrol"

	! use elibc_glibc && {
		epatch "${FILESDIR}/nonglibc_dont_obtain_backtrace.patch" || die
	}

	epatch "${FILESDIR}/fix_dnsmasq_include-${__FTL_PATCH_VERSION__}.patch" || die

	# To do add static USE flag
	epatch "${FILESDIR}/patch_Makefile-${PV}.patch" || die

	# git descriptions setup
	sed -i "s|^GIT_BRANCH := .*$|GIT_BRANCH := $EGIT_BRANCH|w $_ssc" Makefile
	if [ -s $_ssc ] ; then rm $_ssc ; else echo "   ==> Sed error: git descriptions setup 1" && die; fi
	sed -i "s|^GIT_VERSION := .*$|GIT_VERSION := v$PV|w $_ssc" Makefile
	if [ -s $_ssc ] ; then rm $_ssc ; else echo "   ==> Sed error: git descriptions setup 2" && die; fi
	sed -i "s|^GIT_DATE := .*$|GIT_DATE := $(date -I)|w $_ssc" Makefile
	if [ -s $_ssc ] ; then rm $_ssc ; else echo "   ==> Sed error: git descriptions setup 3" && die; fi
	sed -i "s|^GIT_TAG := .*$|GIT_TAG := v$PV|w $_ssc" Makefile
	if [ -s $_ssc ] ; then rm $_ssc ; else echo "   ==> Sed error: git descriptions setup 4" && die; fi

	# setting up logs paths
	#sed -i "s|/var/log/pihole-FTL.log|/run/log/pihole-ftl/pihole-FTL.log|w $_ssc" memory.c
	#if [ -s $_ssc ] ; then rm $_ssc ; else echo "   ==> Sed error: setting up logs paths 1" && die; fi
	sed -i "s|/var/run/pihole-FTL|/run/pihole-ftl/pihole-FTL|w $_ssc" memory.c
	if [ -s $_ssc ] ; then rm $_ssc ; else echo "   ==> Sed error: setting up logs paths 2" && die; fi
	sed -i "s|/var/run/pihole/|/run/pihole-ftl/|w $_ssc" memory.c
	if [ -s $_ssc ] ; then rm $_ssc ; else echo "   ==> Sed error: setting up logs paths 4" && die; fi

	default
}

# this is a crude test with the following limitations:
#  * We end up building pihole-FTL multiple times
#  * Requires network-sandbox to be disabled because we're testing a DNS server...
src_test() {
	emake clean
	# we can't access outside of our little sandbox...
	sed -i "s|/var/log/pihole-FTL.log|${T}/pihole-FTL.log|g" memory.c
	# bind on port 5353 by default since binding to 53 requires root privileges
	sed -i "s|#define NAMESERVER_PORT 53|#define NAMESERVER_PORT 5353|g" dnsmasq/dns-protocol.h
	emake
	./pihole-FTL test || die
	emake clean
	sed -i "s|${T}/pihole-FTL.log|/var/log/pihole-FTL.log|g" memory.c
	sed -i "s|#define NAMESERVER_PORT 5353|#define NAMESERVER_PORT 53|g" dnsmasq/dns-protocol.h
	emake
}

src_install() {
	dobin pihole-FTL

	use systemd && {
		systemd_dounit "${FILESDIR}/${PN}.service"
		systemd_newtmpfilesd "${FILESDIR}/${PN}.tmpfile" "${PN}.conf"
	}

	install -dm755 "${ED}"/etc/pihole
	install -Dm644 "${FILESDIR}/${PN}.conf" "${ED}"/etc/pihole/pihole-FTL.conf

	newinitd "${FILESDIR}/pihole-FTL.initd.10" pihole-FTL

	use man && {
		for f in "${DISTDIR}"/{${PN}-${__FTL_MAN_VERSION__}_pihole-FTL.8,${PN}-${__FTL_MAN_VERSION__}_pihole-FTL.conf.5}
		do
			_f="${f/${PN}-${__FTL_MAN_VERSION__}_/}"
			_f="${_f##*/}"
			newman "$f" "$_f"
		done
	}

	DOC_CONTENTS="
Possible configurations in \e[1;31m/etc/pihole/pihole-FTL.conf\e[0m file
Please read the instructions on the project page: \e[1;36mhttps://github.com/pi-hole/FTL#ftls-config-file\e[0m
"

	readme.gentoo_create_doc
}

pkg_postinst() {
	readme.gentoo_print_elog
	enewuser pihole || die
	touch /var/log/pihole-FTL.log
	chown pihole:pihole /var/log/pihole-FTL.log
	chown pihole:pihole /etc/pihole
	mkdir -p /run/pihole-ftl
	chown pihole:pihole /run/pihole-ftl
	use caps && /sbin/setcap cap_net_bind_service,cap_net_raw,cap_net_admin+ei /usr/bin/pihole-FTL
}

pkg_postrm() {
	id pihole > /dev/null 2>&1 && userdel -f pihole
}

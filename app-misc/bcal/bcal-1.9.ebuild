# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

DESCRIPTION="1234 Storage expression calculator"
HOMEPAGE="https://github.com/jarun/bcal"
SRC_URI="https://github.com/jarun/bcal/archive/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~*"
IUSE="fish-completion doc"

DEPEND="sys-libs/readline:0="
RDEPEND="${DEPEND}"

src_compile() {
	make DESTDIR="${ED}" PREFIX="/usr" $@
}

src_install() {
	src_compile install
	if ! use doc
	then
		rm -rf "${ED}/usr/share/doc"
	else
		mv "${ED}/usr/share/doc/${PN}" "${ED}/usr/share/doc/${PN}-${PV}"
	fi
}

# Copyright 2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Wayland Polybar-like bar for Sway and Wlroots based compositors"
HOMEPAGE="https://github.com/Alexays/Waybar"

inherit git-r3 meson

EGIT_REPO_URI="${HOMEPAGE}.git"

LICENSE="MIT"
SLOT="0"
KEYWORDS=""
IUSE="network pulseaudio tray"

RESTRICT_USE="tray"

DEPEND="
	>=dev-cpp/gtkmm-3.22.2:3.0
	pulseaudio? ( >=media-sound/pulseaudio-12.2 )
	network? ( >=dev-libs/libnl-3.4.0 )
	>=dev-util/gdbus-codegen-2.58.1-r1
	>=dev-libs/libfmt-5.2.1
	>=dev-libs/wayland-1.16.0
	>=dev-libs/wayland-protocols-1.16
"
RDEPEND="${DEPEND}"
BDEPEND=""

src_install() {
	dodoc "README.md"

	meson_src_install
}

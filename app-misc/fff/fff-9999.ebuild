# Copyright 2018 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit git-r3

DESCRIPTION="fucking fast file-manager"
HOMEPAGE="https://github.com/dylanaraps/fff"
EGIT_REPO_URI="${HOMEPAGE}.git"

LICENSE="MIT"
SLOT="0"
KEYWORDS=""
IUSE=""

DEPEND="app-shells/bash:="
RDEPEND="${DEPEND}"
BDEPEND=""

src_install() {
	dodoc README.md
	doman "${PN}.1"
	dobin "${PN}"
}

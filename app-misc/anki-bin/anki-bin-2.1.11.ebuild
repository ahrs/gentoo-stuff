# Copyright 2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="A spaced-repetition memory training program (flash cards)"
HOMEPAGE="https://apps.ankiweb.net"
SRC_URI="https://apps.ankiweb.net/downloads/current/anki-2.1.11-linux-amd64.tar.bz2 -> ${P}.tar.bz2"

inherit xdg

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64"
IUSE="systemd"

DEPEND=""

# Most of the dependencies below were found with ldd
# Some may not be needed.
# To Do:
#	* Find out the exact deps
RDEPEND="
	systemd? ( sys-apps/systemd )
	!app-misc/anki
	${DEPEND}
	app-accessibility/at-spi2-atk
	app-accessibility/at-spi2-core
	app-arch/bzip2
	app-arch/snappy
	app-arch/xz-utils
	app-arch/zstd
	app-crypt/p11-kit
	dev-db/mysql-connector-c
	dev-db/sqlite
	dev-lang/orc
	dev-libs/atk
	dev-libs/double-conversion
	dev-libs/expat
	dev-libs/fribidi
	dev-libs/glib
	dev-libs/gmp
	dev-libs/icu
	dev-libs/libbsd
	dev-libs/libcroco
	dev-libs/libevent
	dev-libs/libffi
	dev-libs/libpcre
	dev-libs/libpcre2
	dev-libs/libressl
	dev-libs/libtasn1
	dev-libs/libunistring
	dev-libs/libxml2
	dev-libs/libxslt
	dev-libs/nettle
	dev-libs/nspr
	dev-libs/nss
	dev-libs/re2
	dev-libs/wayland
	dev-qt/designer:5
	dev-qt/qtbluetooth:5
	dev-qt/qtcore:5
	dev-qt/qtdbus:5
	dev-qt/qtdeclarative:5
	dev-qt/qtgui:5
	dev-qt/qthelp:5
	dev-qt/qtmultimedia:5
	dev-qt/qtnetwork:5
	dev-qt/qtopengl:5
	dev-qt/qtprintsupport:5
	dev-qt/qtsensors:5
	dev-qt/qtsql:5
	dev-qt/qtsvg:5
	dev-qt/qttest:5
	dev-qt/qtwayland:5
	dev-qt/qtwebchannel:5
	dev-qt/qtwebengine:5
	dev-qt/qtwidgets:5
	dev-qt/qtx11extras:5
	dev-qt/qtxml:5
	dev-qt/qtxmlpatterns:5
	gnome-base/librsvg
	media-gfx/graphite2
	media-libs/alsa-lib
	media-libs/fdk-aac
	media-libs/flac
	media-libs/fontconfig
	media-libs/freetype
	media-libs/gst-plugins-base
	media-libs/gstreamer
	media-libs/harfbuzz
	media-libs/lcms
	media-libs/libepoxy
	media-libs/libjpeg-turbo
	media-libs/libogg
	media-libs/libpng
	media-libs/libsndfile
	media-libs/libtheora
	media-libs/libvorbis
	media-libs/libvpx
	media-libs/libwebp
	media-libs/opus
	media-libs/portaudio
	media-libs/x264
	media-libs/x265
	media-libs/xvid
	media-sound/lame
	media-sound/pulseaudio
	media-video/ffmpeg
	net-dns/avahi
	net-dns/libidn2
	net-libs/gnutls
	net-libs/libasyncns
	net-libs/libssh
	net-print/cups
	sys-apps/dbus
	sys-apps/keyutils
	sys-apps/tcp-wrappers
	sys-apps/util-linux
	sys-devel/gcc
	sys-libs/db
	sys-libs/e2fsprogs-libs
	sys-libs/gdbm
	sys-libs/glibc
	sys-libs/libcap
	sys-libs/zlib
	|| (
		virtual/opengl
		x11-drivers/nvidia-drivers
	)
	x11-libs/cairo
	x11-libs/gdk-pixbuf
	x11-libs/gtk+
	x11-libs/libdrm
	x11-libs/libICE
	x11-libs/libSM
	x11-libs/libva
	x11-libs/libvdpau
	x11-libs/libX11
	x11-libs/libXau
	x11-libs/libxcb
	x11-libs/libXcomposite
	x11-libs/libXcursor
	x11-libs/libXdamage
	x11-libs/libXdmcp
	x11-libs/libXext
	x11-libs/libXfixes
	x11-libs/libXi
	x11-libs/libXinerama
	x11-libs/libxkbcommon
	x11-libs/libXrandr
	x11-libs/libXrender
	x11-libs/libXScrnSaver
	x11-libs/libXtst
	x11-libs/pango
	x11-libs/pixman
	x11-libs/xcb-util
	x11-libs/xcb-util-image
	x11-libs/xcb-util-keysyms
	x11-libs/xcb-util-renderutil
	x11-libs/xcb-util-wm
"
BDEPEND=""

S="${WORKDIR}/anki-${PV}-linux-amd64"

src_prepare() {
	sed -i "s|PREFIX=.*|PREFIX=${D}/usr|1" Makefile || die "Failed to patch PREFIX in Makefile"
	# Don't run xdg-mime
	sed -i '/.*xdg-mime.*/d' Makefile

	default
}

src_compile() { :; }

src_install() {
	! use systemd && {
		# Bundled libdbus links against libsystemd :(
		find . -name "libdbus*" -delete || die "Unable to delete libdbus"
	}
	emake install
}

pkg_postinst() {
	xdg_pkg_postinst
}

pkg_postrm() {
	xdg_pkg_postrm
}

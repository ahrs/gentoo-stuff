# Copyright 1999-2018 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Virtual for Cross-platform development framework based on web technologies"
HOMEPAGE=""
SRC_URI=""

LICENSE=""
SLOT="$(ver_cut 1-2)"
KEYWORDS="~amd64 ~arm64 ~x86"

BDEPEND=""
RDEPEND="|| ( `#=dev-util/electron-${PV}*` =dev-util/electron-bin-${PV}* )"

#!/usr/bin/env bash

#set -x

usage() {
  prog="$(basename "$0")"
  cat <<EOF
  $prog - Bash template thingy

  Usage:

  $prog [file]
EOF
}

[[ ! $1 && -t 0 ]] && {
  usage

  exit 1
}

unset usage

f="${1:--}"

[[ $f == "-" ]] && f="/dev/stdin"

# Loads the entire file into memory
# Would bash choke on a large file?
[[ $f == "/dev/stdin" ]] && {
  f=""
  while read ff
  do
    f+="$ff"
  done
} || f="$(< "$f")"

seen_a=0
seen_b=0

b=""

put_var() {
  printf "%s" "${!@}"
}

RET=0

IFS=$'\n'
while read -r f
do
# We can skip iterating character-by-character over every line
# if it doesn't contain {{ SOMEVAR }}. Bash is optimised for globbing
# like this so it is wayyy faster
[[ "$f" != *"{{"*"}}"* ]] && {
  printf "%s\n" "$f"
  continue
}

# reset
seen_a=0
seen_b=0

for ((i = 0; i < ${#f}; i++))
do
  [[ "$seen_a" -eq 0 ]] && [[ "${f:$i:2}" == "{{" ]] && seen_a=1 && i="$((i+1))" && continue
  [[ "$seen_b" -eq 0 ]] && [[ "${f:$i:2}" == "}}" ]] && seen_b=1
  [[ "$seen_b" -eq 1 ]] && {
    i="$((i+1))"
    # With {{ eval(some command) }} arbitrary scripts/commands/executables can
    # be run with their stdout/stderr sent to stdout. Yes, this should be considered
    # "insecure". Use sparingly at your own peril!
    c="${b//eval\(}"
    c="${c%%[[:space:]]}"
    c="${c##[[:space:]]}"
    b="${b//[[:space:]]}"
    if [[ $c != $b ]]
    then 
      c="${c%%[[:space:]]}"
      c="${c%%\)}"
      ! (eval -- "unset put_var;$c" 2>&1) && RET=1
    else
      ! put_var "$b" && RET=1
    fi
    b=""
    c=""
    seen_a=0
    seen_b=0
    continue
  } || {
    [[ "$seen_a" -eq 1 ]] && b+="${f:$i:1}"
  }

  [[ "$seen_a" -eq 0 && "$seen_b" -eq 0 ]] && printf "%s" "${f:$i:1}"
done
  printf "\n"
done <<< "$f"

exit "$RET"

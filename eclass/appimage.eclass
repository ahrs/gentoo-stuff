# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

# @ECLASS: appimage.eclass
# @MAINTAINER:
# ahrs
# @AUTHOR:
# ahrs
# @BLURB: 
# @DESCRIPTION: Provides wrappers for installing an appimage

EXPORT_FUNCTIONS src_install

# By default install into /usr/share/appimages/PACKAGE_NAME/PACKAGE_VERSION
# This allows for slotted ebuilds so more than one appimage version can be
# installed at once.
_E_APPIMAGEDESTREE="/usr/share/appimages/${PN}/${PV}"

S="${DISTDIR}"

appimageinto() {
	export _E_APPIMAGEDESTREE="$1"
}

wrapappimage() {
	local _exe="${1:-$PN-${PV}}"
	newbin - "$_exe" <<EOF
#!/bin/sh
SKIP=true; export SKIP # skip desktop integration
exec $_EAPPIMAGEDEST \$@
EOF
}

doappimage() {
	exeinto "$_E_APPIMAGEDESTREE"
	doexe "$1"
	export _EAPPIMAGEDEST="$_E_APPIMAGEDESTREE/${1##*/}"
}

appimage_src_install() {
	# there's probably a better way to do this?
	local _num_appimages="$(ls -1 "${S}"/*.AppImage | wc -l)"
	[ -z "$_num_appimages" ] && die "There are no AppImages to install"
	[ "$_num_appimages" -ne 1 ] && die "The default appimage_src_install cannot be used with more than one AppImage"
	doappimage "${S}"/*.AppImage
	wrapappimage
}

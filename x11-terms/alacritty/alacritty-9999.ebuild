# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit bash-completion-r1 cargo desktop git-r3

DESCRIPTION="A cross-platform, GPU-accelerated terminal emulator"
HOMEPAGE="https://github.com/jwilm/alacritty"
EGIT_REPO_URI="https://github.com/jwilm/alacritty"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS=""
IUSE="fetch-crates sccache"

# needed for cargo to work :(
RESTRICT="network-sandbox"

CDEPEND="
	media-libs/fontconfig
"
DEPEND="
	$CDEPEND
	>=virtual/rust-1.15.0
"
RDEPEND="${CDEPEND}"

src_test() {
	cargo test
}

src_prepare() {
	# this is intentional, we want the rust eclass
	# to build things not the Makefile
	rm -f Makefile

	default
}

src_compile() {
	use sccache && export RUSTC_WRAPPER=sccache
	cargo_src_compile
}

src_install() {
	#cargo_src_install
	cargo install -j $(makeopts_jobs) --root="${D}/usr" --path . $(usex debug --debug "") || die "cargo install failed"
	[ -f "${D}/usr/.crates.toml" ] && rm -f "${D}/usr/.crates.toml"

	dodoc "${PN}.yml"

	cd extra/completions || die "failed to cd to extra/completions"

	newbashcomp "${PN}.bash" "${PN}"
	insinto /usr/share/zsh/site-functions
	doins "_${PN}"

	insinto /usr/share/fish/vendor_completions.d
	doins "${PN}.fish"

	domenu "../linux/${PN}.desktop"
}

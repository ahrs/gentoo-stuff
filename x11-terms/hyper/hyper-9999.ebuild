# Copyright 2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="A terminal built on web technologies "
HOMEPAGE="https://hyper.is/"

inherit desktop git-r3

EGIT_REPO_URI="https://github.com/zeit/hyper.git"

LICENSE="MIT"
SLOT="0"
KEYWORDS=""
IUSE="test"

DEPEND="
	net-libs/nodejs[npm]
	sys-apps/yarn
"
RDEPEND="${DEPEND}"
BDEPEND=""

src_unpack() {
	git-r3_src_unpack
	cd "${S}"
	#env NODE_ENV=production yarn install
	env yarn install
}

src_test() {
	yarn test || die "Failed to run tests"
}

src_compile() {
	PATH="${PWD}/node_modules/.bin:$PATH"
	npm run build || die
	cross-env BABEL_ENV=production babel \
		--out-file app/renderer/bundle.js \
		--no-comments \
		--minified app/renderer/bundle.js || die
	build --linux --dir
}

src_install() {
	insinto /usr/lib/${PN}
	doins -r dist/linux-unpacked/*
	dodir /usr/bin
	ln -rsv "${ED}/usr/lib/${PN}/${PN}" "${ED}/usr/bin/${PN}"
	chmod +x "${ED}/usr/bin/${PN}"
	newicon app/static/icon.png "${PN}.png"
	newicon -s 96x96 app/static/icon96x96.png "${PN}.png"
	make_desktop_entry hyper "Hyper" hyper "System;TerminalEmulator;"
}

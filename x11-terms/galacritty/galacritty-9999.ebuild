# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit bash-completion-r1 cargo desktop git-r3

DESCRIPTION="WIP GTK terminal emulator based on Alacritty"
HOMEPAGE="https://github.com/myfreeweb/galacritty"
EGIT_REPO_URI="${HOMEPAGE}.git"

LICENSE="
	Apache-2.0 `# Alacritty core`
	Unlicense `# galacritty's license`
"
SLOT="0"
KEYWORDS=""
IUSE="fetch-crates sccache"

# needed for cargo to work :(
RESTRICT="network-sandbox"

CDEPEND="
	media-libs/fontconfig
	x11-libs/gtk+:3
	media-libs/libepoxy
"
DEPEND="
	$CDEPEND
	>=virtual/rust-1.15.0
"
RDEPEND="${CDEPEND}"

src_test() {
	cargo test
}

src_compile() {
	use sccache && export RUSTC_WRAPPER=sccache
	cargo_src_compile
}

src_install() {
	cargo install -j $(makeopts_jobs) --root="${D}/usr" --path . $(usex debug --debug "") || die "cargo install failed"
	[ -f "${D}/usr/.crates.toml" ] && rm -f "${D}/usr/.crates.toml"

	insinto /usr/share
	doins -r res/icons

	make_desktop_entry galacritty Galacritty technology.unrelenting.galacritty.svg "System;TerminalEmulator;"
}

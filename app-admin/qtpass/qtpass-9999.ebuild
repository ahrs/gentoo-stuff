# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6


inherit desktop git-r3 qmake-utils virtualx

DESCRIPTION="multi-platform GUI for pass, the standard unix password manager"
HOMEPAGE="https://qtpass.org/"
#SRC_URI="https://github.com/IJHack/${PN}/archive/master.zip -> ${P}-master.zip"
EGIT_REPO_URI="https://github.com/IJHack/${PN}.git"
LICENSE="GPL-3"
SLOT="0"
KEYWORDS=""
IUSE="test"
DOCS=( FAQ.md README.md CONTRIBUTING.md )

RDEPEND="app-admin/pass
	dev-qt/qtcore:5
	dev-qt/qtgui:5[xcb]
	dev-qt/qtnetwork:5
	dev-qt/qtwidgets:5
	test? ( dev-qt/qttest:5 )
	net-misc/x11-ssh-askpass"
DEPEND="${RDEPEND}
	dev-qt/linguist-tools:5"

src_prepare() {
	default

	#sed -i 's/SUBDIRS += src tests main/SUBDIRS += src main/' "${S}"/qtpass.pro || die
	#sed -i '/main\.depends = tests/d' "${S}"/qtpass.pro || die
}
src_configure() {
	eqmake5 PREFIX="${D}"/usr
}

src_test() {
	virtx default
}

src_install() {
	default

	doman ${PN}.1

	insinto /usr/share/applications
	doins "${PN}.desktop"

	newicon artwork/icon.svg "${PN}-icon.svg"
}

REPOMAN=repoman
EGENCACHE=egencache

all: manifest cache tree yaml

EXCLUDE=""
EBUILD=
EBUILD_ARGS=clean manifest merge
ebuild:
	$@ $(EBUILD) $(EBUILD_ARGS)

manifest:
	$(REPOMAN) $@

cache:
	rm -rf metadata/md5-cache/*
	$(EGENCACHE) --jobs=$(shell expr `nproc` + 1) --update --repo ahrs

yaml:
	./.scripts/generate-gitlab-ci.yml.sh
	@test -f .gitlab-ci.yml && git diff --color=always .gitlab-ci.yml | cat

check:
	$(REPOMAN) full -d

tree:
	sed -i '/^<!-- ::BEGIN_TREE:: -->$$/,$$ d' README.md
	printf '<!-- ::BEGIN_TREE:: -->\n' >> README.md
	tree -d -H 'https://gitlab.com/ahrs/gentoo-stuff/tree/master' -T '' --noreport -I 'files|licenses|profiles|metadata|eclass|patches|$(EXCLUDE)' | pandoc -f html -t markdown_strict | tail +2 >> README.md


# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="A fully-modern text-based browser, rendering to TTY and browsers"
HOMEPAGE="https://www.brow.sh/"
SRC_URI="
	https://github.com/${PN}-org/${PN}/archive/v${PV}.tar.gz -> ${P}.tar.gz
	https://github.com/${PN}-org/${PN}/releases/download/v${PV}/${P}-an.fx.xpi
	https://patch-diff.githubusercontent.com/raw/${PN}-org/${PN}/pull/185.diff -> ${P}_tty-based-scrolling.diff
"

LICENSE="LGPL-2.1"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="upx vanilla"

RESTRICT="network-sandbox"

# the version of go-bindata in go-overlay has a bug so we fetch a version using "go get" instead
# To Do:
#	* fix this
#	dev-go/go-bindata:=
DEPEND="
	dev-go/dep:=
	upx? ( || (
			app-arch/upx-ucl:=
			app-arch/upx-bin:=
		)
	)
"
RDEPEND="www-client/firefox"
BDEPEND=""

src_prepare() {
	if ! use vanilla; then
		patch -p1 -i "${DISTDIR}/${P}_tty-based-scrolling.diff" || die
	fi

	export GOPATH="${T}/.go"
	mkdir -p "${GOPATH}/src"
	ln -s "${S}" "${GOPATH}/src/${PN}"

	local _interfacer="${GOPATH}/src/${PN}/interfacer"
	cd "${_interfacer}" || die "failed to cd to ${_interfacer}"

	local xpipath="$(TMPDIR="$T" mktemp -d)"

	cp "${DISTDIR}/${P}-an.fx.xpi" "${xpipath}/${PN}.xpi" || die

	local _compress=""
	use upx && _compress+="-nocompress"

	go get -u gopkg.in/shuLhan/go-bindata.v3/... || die

	export PATH="$PATH:$GOPATH/bin"

	go-bindata $_compress \
		-prefix "$xpipath" \
		-pkg "${PN}" \
		-o "${_interfacer}/src/${PN}/webextension.go" \
		"${xpipath}/${PN}.xpi" || die

	rm -r "${xpipath}"

	dep ensure -v || die "failed to restore go deps"

	default
}

src_compile() {
	export GOPATH="${T}/.go"

	local _interfacer="${GOPATH}/src/${PN}/interfacer"
	cd "${_interfacer}" || die "failed to cd to ${_interfacer}"

	go build -x \
		-gcflags "all=-trimpath=${GOPATH}" \
		-asmflags "all=-trimpath=${GOPATH}" \
		-o "${S}/${PN}" ./src/main.go || die

	strip --strip-all "${S}/${PN}"

	use upx && upx --best "${S}/${PN}"
}

src_install() {
	dobin "${S}/${PN}"
}

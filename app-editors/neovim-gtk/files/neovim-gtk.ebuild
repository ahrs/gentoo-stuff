# Copyright 2017-{{ eval( printf "%(%Y)T" ) }} Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

CRATES="
{{ eval(curl -s "${CARGO_LOCK_FILE}" | grep -oE '^"checksum\s.*"' | awk '{print $2 "-" $3}') }}"

inherit cargo desktop xdg-utils

DESCRIPTION="neovim-gtk"
HOMEPAGE="https://github.com/daa84/neovim-gtk"

if [[ -z ${PV%%*9999} ]]; then
	inherit git-r3
	EGIT_REPO_URI="${HOMEPAGE}.git"
	KEYWORDS=""
else
	SRC_URI="${HOMEPAGE}/archive/v${PV}.tar.gz -> ${P}.tar.gz"
	KEYWORDS="~amd64"
fi

SRC_URI+="
	$(cargo_crate_uris ${CRATES})
"
RESTRICT="mirror"
LICENSE="GPL-3"
SLOT="0"
IUSE="sccache"

BDEPEND="
	sccache? ( dev-util/sccache )
"
DEPEND="x11-libs/gtk+:3"
RDEPEND="
	app-editors/neovim[remote]
	${DEPEND}
"

src_unpack() {
	cargo_src_unpack
	if [[ -z ${PV%%*9999} ]]; then
		git-r3_src_unpack
	fi
}

src_compile() {
	use sccache && export RUSTC_WRAPPER=sccache
	cargo_src_compile
}

src_install() {
	cargo_src_install
	insinto /usr/share/nvim-gtk
	if ver_test "$PV" -ge 0.1.2
	then
		doins -r runtime
		domenu desktop/org.daa.NeovimGtk.desktop
		newicon -s 48 desktop/org.daa.NeovimGtk_48.png org.daa.NeovimGtk.png
		newicon -s 128 desktop/org.daa.NeovimGtk_128.png org.daa.NeovimGtk.png
		doicon -s scalable desktop/org.daa.NeovimGtk.svg
	else
		domenu ./desktop/nvim-gtk.desktop
		doicon ./desktop/nvim-gtk.{png,svg}
	fi
}

pkg_postinst() {
	xdg_icon_cache_update
	xdg_desktop_database_update
}

pkg_postrm() {
	xdg_icon_cache_update
	xdg_desktop_database_update
}

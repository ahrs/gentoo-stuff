# Copyright 2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit desktop vscode-r1 xdg-utils

DESCRIPTION="Visual Studio Code (Open Source version)"
HOMEPAGE="https://github.com/Microsoft/vscode"

if [[ -z ${PV%%*99999999} ]]; then
	inherit git-r3
	EGIT_REPO_URI="${HOMEPAGE}.git"
	KEYWORDS=""
else
	SRC_URI="${HOMEPAGE}/archive/${PV}.tar.gz -> ${P}.tar.gz"
	KEYWORDS="-* ~amd64 ~x86"
fi

RESTRICT="network-sandbox"

LICENSE="MIT"
SLOT="0"
IUSE="unsupported-node telemetry vanilla"

ELECTRON_V=3.1.3
ELECTRON_SLOT=3.1

RDEPEND="
	x11-libs/libX11
	x11-libs/libxkbfile
	>=app-crypt/libsecret-0.18.5:0[crypt]
	>=net-print/cups-2.1.4:0
	>=x11-libs/libnotify-0.7.7:0
	>=x11-libs/libXScrnSaver-1.2.2-r1:0
	!vanilla? (
		>=virtual/electron-${ELECTRON_V}:${ELECTRON_SLOT}
		sys-apps/ripgrep
	)

"

DEPEND+="${RDEPEND}
	app-misc/jq
	>=dev-lang/python-3.4.5
	unsupported-node? ( >=net-libs/nodejs-8.0.0 )
	!unsupported-node? ( >=net-libs/nodejs-8.0.0 <net-libs/nodejs-10.14.2 )
	sys-apps/yarn
	>=gnome-base/gconf-3.2.6-r4:2
	>=media-libs/libpng-1.2.46:0
	>=x11-libs/cairo-1.14.12:0
	>=x11-libs/gtk+-2.24.31-r1:2
	>=x11-libs/libXtst-1.2.3:0
"
BDEPEND=""

src_unpack() {
	default
	type git-r3_src_unpack > /dev/null 2>&1 && git-r3_src_unpack

	cd "${S}" || die

	# ignore the pre-install checks
	# if the build fails, it fails...
	sed -i 's|if (err)|if (false)|g' build/npm/preinstall.js

	yarn || die
}

src_prepare() {
	cp product.json product.json.bak || die
	jq 'setpath(["extensionsGallery"]; {
			"serviceUrl": "https://marketplace.visualstudio.com/_apis/public/gallery",
			"cacheUrl": "https://vscode.blob.core.windows.net/gallery/index",
			"itemUrl": "https://marketplace.visualstudio.com/items"
	})' product.json > product.json.bak || die
	mv product.json.bak product.json || die

	! use telemetry && {
		/bin/sh -x "${FILESDIR}/"undo_telemetry.sh
		/bin/sh -x "${FILESDIR}/"update_settings.sh
	}

	! use vanilla && {
		eapply "${FILESDIR}/unbundle-ripgrep-r1.patch" && {
			rm -f node_modules/vscode-ripgrep/bin/rg
		} || die "Failed to unbundle ripgrep"
	}

	default
}

src_compile() {
	export NODE_ENV=production

	env NODE_OPTIONS="--max-old-space-size=4096" yarn run gulp vscode-linux-x64-min
}

src_install() {
	dodir /usr/lib
	dodir "/usr/bin"

	if ! use vanilla
	then
		patch -p1 -i "${FILESDIR}/unbundle-electron.patch" "${WORKDIR}/"VSCode-linux-x64/bin/code-oss || die "Failed to apply unbundle-electron.patch"
		sed -i "s|{{ELECTRON_VERSION}}|/usr/bin/electron-$(ver_cut 1-2 ${ELECTRON_V})|g" "${WORKDIR}/"VSCode-linux-x64/bin/code-oss
		ln -sv /usr/lib/code-oss/bin/code-oss "${D}/usr/bin/code-oss"
		rm -f "${WORKDIR}/"VSCode-linux-x64/code-oss
	else
		ln -sv /usr/lib/code-oss/code-oss "${D}/usr/bin/code-oss"
	fi

	mv "${WORKDIR}/"VSCode-linux-x64 "${D}/usr/lib/code-oss"

	sed -i \
		-e 's|@@NAME_LONG@@|Code OSS|g' \
		-e 's|@@NAME@@|code-oss|g' \
		-e 's|@@ICON@@|code-oss|g' \
		-e 's|@@NAME_SHORT@@|code-oss|g' \
		-e 's|/usr/share/code-oss/code-oss|/usr/bin/code-oss|g' "${S}/resources/linux/code.desktop"

	newicon "${S}/resources/linux/code.png" "code-oss.png"
	newmenu "${S}/resources/linux/code.desktop" "code-oss.desktop"
}

pkg_postinst() {
	xdg_desktop_database_update
	xdg_mimeinfo_database_update
}

pkg_postrm() {
	xdg_desktop_database_update
	xdg_mimeinfo_database_update
}

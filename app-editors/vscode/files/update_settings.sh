# Modified from VSCodium (MIT)
# https://github.com/VSCodium/vscodium/blob/master/update_settings.sh

REPLACEMENT="s/'default': true/'default': false/"

sed -i -E "$REPLACEMENT" src/vs/platform/telemetry/common/telemetryService.ts

sed -i -E "$REPLACEMENT" src/vs/workbench/services/crashReporter/electron-browser/crashReporterService.ts

# Copyright 2018 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

PYTHON_COMPAT=( python3_{4,5,6,7} )
inherit distutils-r1

DESCRIPTION="Python Library to control TPLink Switch (HS100 / HS110)"
HOMEPAGE="https://github.com/GadgetReactor/pyHS100"
SRC_URI="${HOMEPAGE}/archive/${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

S="${WORKDIR}/pyHS100-${PV}"

DEPEND="
	dev-python/click-datetime[$PYTHON_USEDEP]
	dev-python/typing[$PYTHON_USEDEP]
"
RDEPEND="${DEPEND}"
BDEPEND=""

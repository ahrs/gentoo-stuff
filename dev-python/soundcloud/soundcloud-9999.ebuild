# Copyright 2018 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

PYTHON_COMPAT=( python2_7 python3_6 )
inherit distutils-r1 git-r3

DESCRIPTION="A Python wrapper around the Soundcloud API"
HOMEPAGE="https://github.com/soundcloud/soundcloud-python"

EGIT_REPO_URI="${HOMEPAGE}.git"

LICENSE="BSD-2"
SLOT="0"
KEYWORDS=""
IUSE=""

DEPEND="
	dev-python/setuptools[$PYTHON_USEDEP]
	dev-python/simplejson[$PYTHON_USEDEP]
	dev-python/requests[$PYTHON_USEDEP]
"
RDEPEND="${DEPEND}"
BDEPEND=""

# Copyright 2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="desktop components for Wayfire"
HOMEPAGE="https://github.com/WayfireWM/wf-shell"

EGIT_REPO_URI="${HOMEPAGE}.git"

inherit git-r3 meson

LICENSE="MIT"
SLOT="0"
KEYWORDS=""
IUSE=""

DEPEND="
	dev-libs/wayland
	dev-libs/wayland-protocols
	gui-libs/wf-config
	dev-cpp/gtkmm:3.0
"
RDEPEND="${DEPEND}"
BDEPEND=""

src_install() {
	meson_src_install
	dodoc "${PN}.ini.example"
}

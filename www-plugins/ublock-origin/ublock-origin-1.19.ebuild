# Copyright 2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Ublock Origin web extension (Debian package)"
HOMEPAGE="https://salsa.debian.org/webext-team/ublock-origin"
SRC_URI="
	!bindist? (
		mirror://debian/pool/main/u/ublock-origin/webext-ublock-origin_1.19.0+dfsg-1_all.deb
		mirror://debian/pool/main/u/ublock-origin/chromium-ublock-origin_1.19.0+dfsg-1_all.deb
	)
"

LICENSE="BSD-2 CC-BY-SA-3.0 GPL-3+ LGPL-3 MPL-2.0 OFL-1.1"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="bindist"

# To Do:
#	* Remove this restriction and build from source when bindist is not set
REQUIRED_USE="!bindist"

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND=""

inherit unpacker

src_unpack() {
	if ! use bindist
	then
		cd "${WORKDIR}" || die
		for deb in "${DISTDIR}"/*.deb
		do
			unpack_deb "$deb" || die
		done
	else
		default
	fi
}

S="${WORKDIR}"
src_install() {
	# Comply with Gentoo FHS
	pushd usr/share/doc || die
	mkdir ".${P}"
	mv ./* ".${P}"
	mv ".${P}" "${P}"
	popd || die
	if [ -d ./usr/share/lintian ]
	then
		rm -rfv ./usr/share/lintian || die
	fi
	doins -r usr
}

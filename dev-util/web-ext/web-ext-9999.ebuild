# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

if [[ -z ${PV%%*9999} ]]; then
	inherit git-r3
	EGIT_REPO_URI="https://github.com/mozilla/web-ext.git"
else
	SRC_URI="
		https://github.com/mozilla/web-ext/archive/${PV}.tar.gz -> ${P}.tar.gz
	"
	RESTRICT="primaryuri"
	KEYWORDS="~amd64 ~x86"
fi

inherit multilib

RESTRICT="${RESTRICT} network-sandbox"

DESCRIPTION="A command line tool to help build, run, and test web extensions"
HOMEPAGE="https://github.com/mozilla/web-ext"

LICENSE="MPL-2.0"
SLOT="0"
IUSE=""

DEPEND="
	net-libs/nodejs[npm]
"
RDEPEND="${DEPEND}"
DOCS=( README.md )

src_compile() {
	export HOME="${T}"
	npm install grunt-cli || die "Failed to npm install grunt"
	[ ! -f "./node_modules/.bin/grunt" ] && die "grunt not found"
	export NODE_ENV=production
	export PATH="${PATH}:$PWD/node_modules/.bin"
	npm install --production || die "npm install failed"
	npm run build || die "npm run build failed"
	rm -rf node_modules || die "Failed to remove node_modules"
	npm install --production || die "production npm install failed"
}

src_install() {
	export NODE_ENV="production"
	export HOME="${T}"
	if ! libdir="$(get_libdir)"
	then
		die "get_libdir failed"
	fi

	dodir "/usr/$libdir/node_modules/web-ext"

	chmod +x bin/${PN}

	cp -r ./dist "${ED}/usr/$libdir/node_modules/web-ext"/
	cp -r ./bin "${ED}/usr/$libdir/node_modules/web-ext"/
	cp -r ./node_modules "${ED}/usr/$libdir/node_modules/web-ext"/
	cp ./package.json "${ED}/usr/$libdir/node_modules/web-ext"/

	dosym "/usr/$libdir/node_modules/web-ext/bin/web-ext" /usr/bin/web-ext

	einstalldocs
}

#!/sbin/openrc-run
# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

export TERM=xterm

command="/var/lib/Jackett/JackettConsole"
extra_started_commands="reload"
description="API Support for your favorite torrent trackers."
description_start="Start Jackett"
description_stop="Stop Jackett"

rundir=${rundir:-/var/run/jackett}
pidfile=${pidfile:-${rundir}/jackett.pid}
runas_user=${runas_user:-jackett:jackett}
logfile=${logfile:-/var/log/jackett/jackett.log}
JACKETT_PORT=${JACKETT_PORT:-9117}
JACKETT_OPTIONS=${JACKETT_OPTIONS:---ListenPublic}

retry="TERM/45/QUIT/15"

start_pre() {
	# Call mkdir -p in case parent dirs are missing
	mkdir -p "${rundir}"

	# Call checkpath to fixup permissions
	checkpath -d -o "${runas_user}" "${rundir}" || return
	
	local _logdir=""
	_logdir="$(dirname "$logfile")"

	# set the permissions for the default logfile
	[ "$_logdir" = "/var/log/jackett" ] && {
		[ ! -d "/var/log/jackett" ] && mkdir -p "/var/log/jackett"
		checkpath -d -o "${runas_user}" "/var/log/jackett"
		[ ! -d "$logfile" ] && {
			touch "$logfile"
			chown "${runas_user}" "$logfile"
		}
	}

	[ -d "$_logdir" ] && {
		# move old log files to $logdir
		find -L /var/lib/Jackett -name "log.*txt" -not -name "log.txt" | while read -r line
		do
			local _f=""
			local _log=""
			# we could use bash's substitutions here but it doesn't work in dash
			_f="$(printf "%s" "$line" | sed 's|log.|jackett.|')"
			_log="$_logdir/$(basename "$_f")"
			mv "$line" "$_log"
		done
	}

	mkdir -p "/var/lib/Jackett/.config/Jackett"
	checkpath -d -o "${runas_user}" "/var/lib/Jackett/.config/Jackett"
	ln -fs /var/lib/Jackett/.config/Jackett/log.txt "$logfile"
}

start() {
	start-stop-daemon -d /var/lib/Jackett -u "${runas_user}" --start --background --pidfile $pidfile --exec "$command" -- --PIDFile $pidfile --Port $JACKETT_PORT --NoUpdates $JACKETT_OPTIONS

	eend $?
}

stop() {
	start-stop-daemon --stop --pidfile $pidfile --retry 15
}

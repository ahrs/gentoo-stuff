# Copyright {{ eval( printf "%(%Y)T" ) }} Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

CRATES="
{{ eval(curl -s ${CARGO_LOCK_FILE} | grep -oE '^"checksum\s.*"' | awk '{print $2 "-" $3}') }}"

inherit cargo

DESCRIPTION="Meta upgrade tool for pip, flatpak, your distro and everything else"
HOMEPAGE="https://github.com/r-darwish/topgrade"

if [[ -z ${PV%%*9999} ]]; then
	inherit git-r3
	EGIT_REPO_URI="${HOMEPAGE}.git"
else
	SRC_URI="${HOMEPAGE}/archive/v${PV}.tar.gz -> ${P}.tar.gz"
fi

SRC_URI+="
	$(cargo_crate_uris ${CRATES})
"

LICENSE="MPL-2.0"
SLOT="0"

IUSE="fetch-crates sccache"

BDEPEND="
	sccache? ( dev-util/sccache )
"

if [[ ! -z ${PV%%*9999} ]]; then
	KEYWORDS="~amd64 ~x86"
fi

src_unpack() {
	cargo_src_unpack
	if [[ -z ${PV%%*9999} ]]; then
		git-r3_src_unpack
	fi
}

src_compile() {
	use sccache && export RUSTC_WRAPPER=sccache
	cargo_src_compile
}

src_install() {
	dobin target/$(usex debug debug release)/${PN}
}

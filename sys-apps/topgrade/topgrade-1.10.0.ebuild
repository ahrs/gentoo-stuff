# Copyright 2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

CRATES="
MacTypes-sys-2.1.0
adler32-1.0.3
aho-corasick-0.6.10
ansi_term-0.11.0
arrayvec-0.4.10
atty-0.2.11
autocfg-0.1.2
backtrace-0.3.14
backtrace-sys-0.1.28
base64-0.10.1
base64-0.9.3
bitflags-1.0.4
build_const-0.2.1
byteorder-1.3.1
bytes-0.4.12
bzip2-0.3.3
bzip2-sys-0.1.7
cc-1.0.31
cfg-if-0.1.7
clap-2.32.0
clicolors-control-1.0.0
cloudabi-0.0.3
console-0.7.5
core-foundation-0.5.1
core-foundation-sys-0.5.1
crc-1.8.1
crc32fast-1.2.0
crossbeam-deque-0.7.1
crossbeam-epoch-0.7.1
crossbeam-queue-0.1.2
crossbeam-utils-0.6.5
directories-1.0.2
dtoa-0.4.3
either-1.5.1
encode_unicode-0.3.5
encoding_rs-0.8.17
env_logger-0.6.1
failure-0.1.5
failure_derive-0.1.5
filetime-0.2.4
flate2-1.0.7
fnv-1.0.6
foreign-types-0.3.2
foreign-types-shared-0.1.1
fuchsia-cprng-0.1.1
fuchsia-zircon-0.3.3
fuchsia-zircon-sys-0.3.3
futures-0.1.25
futures-cpupool-0.1.8
h2-0.1.17
heck-0.3.1
http-0.1.16
httparse-1.3.3
humantime-1.2.0
hyper-0.12.25
hyper-old-types-0.11.0
hyper-tls-0.3.1
idna-0.1.5
indexmap-1.0.2
iovec-0.1.2
itoa-0.4.3
kernel32-sys-0.2.2
language-tags-0.2.2
lazy_static-1.3.0
lazycell-1.2.1
libc-0.2.50
libflate-0.1.21
lock_api-0.1.5
log-0.4.6
matches-0.1.8
memchr-2.2.0
memoffset-0.2.1
mime-0.3.13
mime_guess-2.0.0-alpha.6
miniz-sys-0.1.11
miniz_oxide-0.2.1
miniz_oxide_c_api-0.2.1
mio-0.6.16
miow-0.2.1
native-tls-0.2.2
net2-0.2.33
nix-0.13.0
nodrop-0.1.13
num_cpus-1.10.0
openssl-0.10.19
openssl-probe-0.1.2
openssl-sys-0.9.42
owning_ref-0.4.0
parking_lot-0.7.1
parking_lot_core-0.4.0
pbr-1.0.1
percent-encoding-1.0.1
phf-0.7.24
phf_codegen-0.7.24
phf_generator-0.7.24
phf_shared-0.7.24
pkg-config-0.3.14
podio-0.1.6
proc-macro2-0.4.27
quick-error-1.2.2
quote-0.6.11
rand-0.4.6
rand-0.6.5
rand_chacha-0.1.1
rand_core-0.3.1
rand_core-0.4.0
rand_hc-0.1.0
rand_isaac-0.1.1
rand_jitter-0.1.3
rand_os-0.1.3
rand_pcg-0.1.2
rand_xorshift-0.1.1
rdrand-0.4.0
redox_syscall-0.1.51
redox_termios-0.1.1
regex-1.1.2
regex-syntax-0.6.5
remove_dir_all-0.5.1
reqwest-0.9.11
rustc-demangle-0.1.13
rustc_version-0.2.3
ryu-0.2.7
safemem-0.3.0
same-file-1.0.4
schannel-0.1.15
scopeguard-0.3.3
security-framework-0.2.2
security-framework-sys-0.2.3
self_update-0.5.1
semver-0.9.0
semver-parser-0.7.0
serde-1.0.89
serde_derive-1.0.89
serde_json-1.0.39
serde_urlencoded-0.5.4
shellexpand-1.0.0
siphasher-0.2.3
slab-0.4.2
smallvec-0.6.9
stable_deref_trait-1.1.1
string-0.1.3
strsim-0.7.0
structopt-0.2.15
structopt-derive-0.2.15
syn-0.15.29
synstructure-0.10.1
tar-0.4.22
tempdir-0.3.7
tempfile-3.0.7
termcolor-1.0.4
termion-1.5.1
termios-0.3.1
textwrap-0.10.0
thread_local-0.3.6
time-0.1.42
tokio-0.1.17
tokio-current-thread-0.1.5
tokio-executor-0.1.6
tokio-io-0.1.12
tokio-reactor-0.1.9
tokio-sync-0.1.4
tokio-tcp-0.1.3
tokio-threadpool-0.1.12
tokio-timer-0.2.10
tokio-trace-core-0.1.0
toml-0.4.10
try-lock-0.2.2
ucd-util-0.1.3
unicase-1.4.2
unicase-2.3.0
unicode-bidi-0.3.4
unicode-normalization-0.1.8
unicode-segmentation-1.2.1
unicode-width-0.1.5
unicode-xid-0.1.0
url-1.7.2
utf8-ranges-1.0.2
uuid-0.7.2
vcpkg-0.2.6
vec_map-0.8.1
version_check-0.1.5
void-1.0.2
walkdir-2.2.7
want-0.0.6
which-2.0.1
winapi-0.2.8
winapi-0.3.6
winapi-build-0.1.1
winapi-i686-pc-windows-gnu-0.4.0
winapi-util-0.1.2
winapi-x86_64-pc-windows-gnu-0.4.0
wincolor-1.0.1
ws2_32-sys-0.2.1
xattr-0.2.2
zip-0.5.1
"

inherit cargo

DESCRIPTION="Meta upgrade tool for pip, flatpak, your distro and everything else"
HOMEPAGE="https://github.com/r-darwish/topgrade"

if [[ -z ${PV%%*9999} ]]; then
	inherit git-r3
	EGIT_REPO_URI="${HOMEPAGE}.git"
else
	SRC_URI="${HOMEPAGE}/archive/v${PV}.tar.gz -> ${P}.tar.gz"
fi

SRC_URI+="
	$(cargo_crate_uris ${CRATES})
"

LICENSE="MPL-2.0"
SLOT="0"

IUSE="fetch-crates sccache"

BDEPEND="
	sccache? ( dev-util/sccache )
"

if [[ ! -z ${PV%%*9999} ]]; then
	KEYWORDS="~amd64 ~x86"
fi

src_unpack() {
	cargo_src_unpack
	if [[ -z ${PV%%*9999} ]]; then
		git-r3_src_unpack
	fi
}

src_compile() {
	use sccache && export RUSTC_WRAPPER=sccache
	cargo_src_compile
}

src_install() {
	dobin target/$(usex debug debug release)/${PN}
}

# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="The XBPS package system utilities"
HOMEPAGE="https://github.com/void-linux/xbps"

if [[ -z ${PV%%*9999} ]]; then
	inherit git-r3
	EGIT_REPO_URI="https://github.com/void-linux/${PN}.git"
else
	SRC_URI="https://github.com/void-linux/${PN}/archive/${PV}.tar.gz -> ${P}.tar.gz"
	RESTRICT="mirror"
fi

LICENSE="BSD-2"
SLOT="0"

# only tested on amd64 but should compile on other arch's
# since Void has official iso's for these platforms
KEYWORDS=""
IUSE="test debug ssl libressl"

PATCHES=(
	"${FILESDIR}/fix-confdir-size.patch"
	"${FILESDIR}/fix-rootdir-size.patch"
	"${FILESDIR}/fix-strncat-dash.patch"
)

DEPEND="
	sys-libs/zlib
	ssl? (
		!libressl? (
			dev-libs/openssl
		)
	)
	libressl? (
		dev-libs/libressl
	)
	app-arch/libarchive
	test? (
		dev-libs/atf
		dev-util/kyua
	)
"
RDEPEND="
	app-misc/ca-certificates
"
BDEPEND=""

src_configure() {
	local config_args=()
	use test && config_args+=(--enable-tests)
	use debug && config_args+=(--enable-debug)

	econf ${config_args[@]}
}

# Copyright 2017-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

CRATES="
aho-corasick-0.6.4
ansi_term-0.11.0
assert_matches-1.2.0
atty-0.2.10
backtrace-0.3.8
backtrace-sys-0.1.23
bitflags-1.0.3
brev-0.1.14
cc-1.0.17
cfg-if-0.1.3
clap-2.31.2
ctrlc-3.1.1
dotenv-0.13.0
edit-distance-2.0.1
either-1.5.0
env_logger-0.5.12
executable-path-1.0.0
failure-0.1.1
failure_derive-0.1.1
fuchsia-zircon-0.3.3
fuchsia-zircon-sys-0.3.3
glob-0.2.11
humantime-1.1.1
itertools-0.7.8
lazy_static-1.0.1
libc-0.2.42
log-0.4.4
memchr-2.0.1
nix-0.11.0
quick-error-1.2.2
quote-0.3.15
rand-0.4.2
redox_syscall-0.1.40
redox_termios-0.1.1
regex-1.0.0
regex-syntax-0.6.1
remove_dir_all-0.5.1
rustc-demangle-0.1.8
strsim-0.7.0
syn-0.11.11
synom-0.11.3
synstructure-0.6.1
target-1.0.0
tempdir-0.3.7
termcolor-1.0.1
termion-1.5.1
textwrap-0.9.0
thread_local-0.3.5
ucd-util-0.1.1
unicode-width-0.1.5
unicode-xid-0.0.4
unreachable-1.0.0
utf8-ranges-1.0.0
vec_map-0.8.1
void-1.0.2
winapi-0.3.5
winapi-i686-pc-windows-gnu-0.4.0
winapi-x86_64-pc-windows-gnu-0.4.0
wincolor-1.0.0
"

inherit cargo multiprocessing

DESCRIPTION="Just a command runner"
HOMEPAGE="https://github.com/casey/just"
EGIT_REPO_URI="${HOMEPAGE}.git"
LICENSE="CC0-1.0"
SLOT="0"

if [[ -z ${PV%%*9999} ]]; then
	inherit git-r3
	EGIT_REPO_URI="${HOMEPAGE}.git"
else
	SRC_URI="${HOMEPAGE}/archive/v${PV}.tar.gz -> ${P}.tar.gz"
fi

SRC_URI+="
	$(cargo_crate_uris ${CRATES})
"

IUSE="debug sccache"

BDEPEND="
	sccache? ( dev-util/sccache )
"
DEPEND=""
RDEPEND="${DEPEND}"

# The following tests currently fail:
#    env_var_functions::bash
#    env_var_functions::dash
#    env_var_functions::sh
RESTRICT="test"

src_unpack() {
	cargo_src_unpack
	if [[ -z ${PV%%*9999} ]]; then
		git-r3_src_unpack
	fi
}

src_compile() {
	use sccache && export RUSTC_WRAPPER=sccache
	cargo_src_compile
}

src_test() {
	./target/$(usex debug "debug" "release")/just test || die "test failed"
}

src_install() {
	cargo_src_install
}

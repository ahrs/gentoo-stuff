# Copyright 2017-{{ eval( printf "%(%Y)T" ) }} Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

CRATES="
{{ eval(curl -s "$CARGO_LOCK_FILE" | grep -oE '^"checksum\s.*"' | awk '{print $2 "-" $3}') }}"

inherit cargo multiprocessing

DESCRIPTION="Just a command runner"
HOMEPAGE="https://github.com/casey/just"
EGIT_REPO_URI="${HOMEPAGE}.git"
LICENSE="CC0-1.0"
SLOT="0"

if [[ -z ${PV%%*9999} ]]; then
	inherit git-r3
	EGIT_REPO_URI="${HOMEPAGE}.git"
else
	SRC_URI="${HOMEPAGE}/archive/v${PV}.tar.gz -> ${P}.tar.gz"
fi

SRC_URI+="
	$(cargo_crate_uris ${CRATES})
"

IUSE="debug sccache"

BDEPEND="
	sccache? ( dev-util/sccache )
"
DEPEND=""
RDEPEND="${DEPEND}"

# The following tests currently fail:
#    env_var_functions::bash
#    env_var_functions::dash
#    env_var_functions::sh
RESTRICT="test"

src_unpack() {
	cargo_src_unpack
	if [[ -z ${PV%%*9999} ]]; then
		git-r3_src_unpack
	fi
}

src_compile() {
	use sccache && export RUSTC_WRAPPER=sccache
	cargo_src_compile
}

src_test() {
	./target/$(usex debug "debug" "release")/just test || die "test failed"
}

src_install() {
	cargo_src_install
}

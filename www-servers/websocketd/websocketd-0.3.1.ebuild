# Copyright 2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Like inetd but for websockets"
HOMEPAGE="http://websocketd.com/"

if [[ -z ${PV%%*9999} ]]; then
	inherit git-r3
	EGIT_REPO_URI="https://github.com/joewalnes/${PN}.git"
	KEYWORDS=""
else
	SRC_URI="https://github.com/joewalnes/websocketd/archive/v${PV}.tar.gz -> ${P}.tar.gz"
	KEYWORDS="~amd64 ~x86"
fi

inherit xdg-utils

LICENSE="BSD-2"
SLOT="0"
IUSE="examples"
RESTRICT="network-sandbox"

DEPEND="dev-lang/go"
RDEPEND="${DEPEND}"
BDEPEND=""

src_compile() {
	local EGO_BUILD_FLAGS=""
	xdg_environment_reset
	GOPATH="${T}/.go"
	GOCACHE="${T}/go-cache"
	go build -v -work -x ${EGO_BUILD_FLAGS} . || die
}

src_install() {
	dobin "${PN}" || die
	newman release/websocketd.man "${PN}.8"
	use examples && {
		docompress -x "/usr/share/doc/${P}/examples"
		dodoc -r examples
	}
}

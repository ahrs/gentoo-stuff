# Copyright 2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="The EGLStream-based Wayland external platform"
HOMEPAGE="https://github.com/NVIDIA/egl-wayland"

inherit git-r3 meson multilib

EGIT_REPO_URI="${HOMEPAGE}.git"

LICENSE="NVIDIA-${PN}"
SLOT="0"
KEYWORDS=""
IUSE=""

DEPEND="
	dev-libs/wayland
	dev-libs/wayland-protocols
	media-libs/mesa[egl]
	dev-libs/eglexternalplatform
"
RDEPEND="${DEPEND} x11-drivers/nvidia-drivers[wayland]"
BDEPEND=""

src_install() {
	meson_src_install
	# The nvidia-drivers package provides the library so we remove it here to avoid file conflicts
	rm -rf "${ED}/usr/$(get_libdir)/"libnvidia-egl-wayland.so*
}

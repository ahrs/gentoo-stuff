# Copyright 2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Customisable wayland compositor based on wlroots"
HOMEPAGE="https://github.com/WayfireWM/wayfire"

inherit git-r3 meson

EGIT_REPO_URI="${HOMEPAGE}.git"

LICENSE="MIT"
SLOT="0"
KEYWORDS=""
IUSE="shell"

DEPEND="
	media-libs/glm
	dev-libs/wlroots
	dev-libs/wf-config
"
RDEPEND="
	${DEPEND}
	shell? ( dev-libs/wf-shell )
"
BDEPEND=""

src_install() {
	meson_src_install
	dodoc "${PN}.ini.default"
}

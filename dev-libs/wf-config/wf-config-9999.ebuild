# Copyright 2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="wf-config dependency for wayfire"
HOMEPAGE="https://github.com/ammen99/wf-config"

EGIT_REPO_URI="${HOMEPAGE}.git"

inherit git-r3 meson

LICENSE="MIT"
SLOT="0"
KEYWORDS=""
IUSE=""

DEPEND="
	dev-libs/libevdev
	dev-libs/wlroots
"
RDEPEND="${DEPEND}"
BDEPEND=""

# Copyright 2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="The EGL External Platform interface"
HOMEPAGE="https://github.com/NVIDIA/eglexternalplatform"

inherit git-r3

EGIT_REPO_URI="${HOMEPAGE}.git"

LICENSE="NVIDIA-eglexternalplatform"
SLOT="0"
KEYWORDS=""

IUSE="examples"

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND=""

src_compile() {
	:
}

src_install() {
	use examples && {
		docompress -x /usr/share/doc/${P}/samples
		insinto /usr/share/doc/${P}
		doins -r samples
	}
	insinto /usr/share/pkgconfig
	doins eglexternalplatform.pc
	insinto /usr/include
	doins interface/*
}
